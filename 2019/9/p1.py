import argparse
from itertools import permutations
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

class IntCpu(object):
  def __init__(self, verbose, debug):
    self.memory = []
    self.relative_base = 0
    self.input = []
    self.ip = 0
    self.halted = True
    self.verbose = verbose
    self.debug = debug
    self.ops = {
      1: (self.opcode_add, 3),
      2: (self.opcode_multi, 3),
      3: (self.opcode_input, 1),
      4: (self.opcode_output, 1),
      5: (self.opcode_jump_if_true, 2),
      6: (self.opcode_jump_if_false, 2),
      7: (self.opcode_less_than, 3),
      8: (self.opcode_equals, 3),
      9: (self.opcode_adjust_relative_base, 1),
      99: (self.opcode_halt, 0)
    }

    self.param_modes =  {
      0: self.param_position,
      1: self.param_immediate,
      2: self.param_relative
    }

  def log(self, x):
    if self.verbose is True:
      print(x)

  def read(self, i):
    if i < len(self.memory):
      return self.memory[i]
    else:
      return 0

  def write(self, i, value):
    if i >= len(self.memory):
      self.memory.extend([0] * ( i - len(self.memory) + 1))
    self.memory[i] = value

  def load_code(self, filename):
    with open(filename) as file:
      for line in file.readlines():
        codes = line.split(',')
        for code in codes:
          self.memory.append(int(code))

  def load_input(self, input):
    self.input = [input] + self.input

  def resolve(self, p, dest_addr = False):
    r = p[0](p[1], dest_addr=dest_addr)
    return (int(r[0]), r[1])

  def opcode_add(self, x, y, dest):
    d, ds = self.resolve(dest, True)
    x, xs = self.resolve(x)
    y, ys = self.resolve(y)
    result = x + y
    self.log("{}: ADD {} ({}) and {} ({}) = {} to {} ({})".format(self.ip, x, xs, y, ys, result, d, ds))
    self.write(d, result)
    self.ip += 4
    return result

  def opcode_multi(self, x, y, dest):
    d, ds = self.resolve(dest, True)
    x, xs = self.resolve(x)
    y, ys = self.resolve(y)
    result = x * y
    self.log("{}: MULTIPLY {} ({}) and {} ({}) = {} to {} ({})".format(self.ip, x, xs, y, ys, result, d, ds))
    self.write(d, result)
    self.ip += 4
    return result

  def opcode_halt(self):
    self.halted = True
    self.log("{}: HALT".format(self.ip))

  def opcode_input(self, dest):
    d, d_s = self.resolve(dest, dest_addr=True)
    value = self.input.pop()
    self.write(d, value)
    self.log("{}: INPUT {} to {} ({})".format(self.ip, value, d, d_s))
    self.ip += 2
    return value

  def opcode_output(self, source):
    src, src_s = self.resolve(source)
    self.log("{}: OUTPUT {} ({})".format(self.ip, src, src_s))
    self.ip += 2
    return src

  def opcode_jump_if_true(self, cond, target):
    c, cs = self.resolve(cond)
    t, ts = self.resolve(target)
    start_ip = self.ip

    if c != 0:
      self.ip = t
      r =  t
    else:
      self.ip += 3
      r = None
    self.log("{}: JUMP to {} ({}) IF {} ({}) is TRUE - {}".format(start_ip, t, ts, c, cs, r))
  def opcode_jump_if_false(self, cond, target):
    c, cs = self.resolve(cond)
    t, ts = self.resolve(target)
    start_ip = self.ip
    if c == 0:
      self.ip = t
      r = t
    else:
      self.ip += 3
      r = None
    self.log("{}: JUMP to {} ({}) IF {} ({}) is FALSE - {}".format(start_ip, t, ts, c, cs, r))

  def opcode_less_than(self, left, right, dest):
    l, l_s = self.resolve(left)
    r, r_s = self.resolve(right)
    d, d_s = self.resolve(dest, dest_addr=True)
    result = 1 if l < r else 0
    self.log("{}: {} put {} ({}) LESS THAN {} ({}) - {} - into {} ({})".format(self.ip, self.read(self.ip), l, l_s, r, r_s, result, d, d_s))
    self.write(d, result)
    self.ip += 4
    return result

  def opcode_equals(self, left, right, dest):
    l, l_s = self.resolve(left)
    r, r_s = self.resolve(right)
    d, d_s = self.resolve(dest, dest_addr=True)
    result = 1 if l == r else 0
    self.log("{}: put {} ({}) EQUALS {} ({}) - {} - into {} ({})".format(self.ip, l, l_s, r, r_s, result, d, d_s))
    self.write(d,  result)
    self.ip += 4
    return result

  def opcode_adjust_relative_base(self, value):
    val, val_s = self.resolve(value)
    result = self.relative_base + val
    self.log("{}: ADJUST BASE from {} by {} ({})= {}".format(self.ip, self.relative_base, val, val_s, result))
    self.relative_base += val
    self.ip += 2
    return self.relative_base

  def param_position(self, value, dest_addr):
    if dest_addr:
      return value, "p{}d".format(value)
    return self.read(value), "p{}".format(value)

  def param_immediate(self, value, dest_addr):
    if dest_addr:
      return self.read(value), "i{}d".format(value, dest_addr)
    return value, "i{}".format(value)

  def param_relative(self, value, dest_addr):
   if dest_addr:
     return value + self.relative_base, "r{}+{}d".format(value, self.relative_base)
   result = self.read(value + self.relative_base)
   return result, "r{}+{}".format(value, self.relative_base)

  def run(self):
    self.halted = False
    while self.halted is False:
      opcode_whole = str( self.read(self.ip) )
      mode_part = opcode_whole[:-2]
      opcode = int(opcode_whole[-2:])

      op, p_count = self.ops[opcode]
      mode_part = (("0" * p_count) + mode_part)[-p_count:]
      params = self.memory[self.ip + 1: self.ip + p_count + 1]

      for i in range(len(params)):
        mode = int(mode_part[-(i+1)])
        p = (self.param_modes[mode], int(params[i]))
        params[i] = p

      output = op(*params)
      if op == self.opcode_output:
        #print(">>", output)
        yield output
      while self.debug is True:
        cmd = input(">")
        if cmd.startswith('mem'):
          c, s, e = cmd.split(' ')
          self.log(self.memory[int(s):int(e)])
        if not cmd:
          break
def do_it(args):

  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  for x in args.input:
    cpu.load_input(int(x))
  for output in cpu.run():
    print(output)


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")
  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

import argparse
from collections import Counter
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      for c in line:
        if c != '\n':
          yield int(c)

def do_it(args):
  data = load_data(args.filename)
  width, height = (25, 6)

  final_image = {}
  try:
    while True:
      layer = {}
      counter = Counter()
      for r in range(height):
        for c in range(width):
          color = next(data)
          if color != 2 and (c, r) not in final_image:
            final_image[(c,r)] = color

  except StopIteration as si:
    print("Done")
  x = {0: ' ', 1: '+', 2: '.'}
  for r in range(height):
    for c in range(width):
      print(x[final_image[(c,r)]], end="")
    print()
if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

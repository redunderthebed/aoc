import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph

def load_data(filename):
  print("Loadin' up the old data")
  with open(filename) as file:
    print("opened up the file")
    for line in file.readlines():
      match = re.search('<x=(-?\d+), y=(-?\d+), z=(-?\d+)', line)
      yield [int(x) for x in match.groups()]

X,Y,Z = 0,1,2

class Body(object):
  BOD_COUNT = 0
  def __init__(self, x, y, z):
    Body.BOD_COUNT += 1
    self.id = Body.BOD_COUNT
    self.pos = [x,y,z]
    self.vel = [0,0,0]

  def gravitate(self, other):
    for axis in [X,Y,Z]:
      if other.pos[axis] - self.pos[axis] > 0:
        self.vel[axis] += 1
      elif other.pos[axis] - self.pos[axis] < 0:
        self.vel[axis] -= 1

  def momentivize(self):
    for axis in [X,Y,Z]:
      self.pos[axis] += self.vel[axis]

  def info(self):
    return "{}:pos=<x={:3d}, y={:3d}, z={:3d} vel=<x={:3d}, y={:3d}, z={:3d}>: Total Juice - {}".format(self.id, *(self.pos + self.vel + [self.total_juice()]))

  def total_juice(self):
    return sum(abs(x) for x in self.pos) * sum(abs(x) for x in self.vel)

  def __repr__(self):
    return "Bod {}".format(self.id)

def do_it(args):
  print(args.filename)
  bods = []
  for x,y,z in load_data(args.filename):
    bods.append(Body(x,y,z))

  print("After 0 steps")
  for bod in bods:
    print(bod.info())

  for steps in range(1, 1+(args.count or 10)):
    print("After {} steps".format(steps))
    for combo in combinations(bods, 2):
      combo[0].gravitate(combo[1])
      combo[1].gravitate(combo[0])

    for bod in bods:
     bod.momentivize()
     print(bod.info())

  print("Total Energy: {}".format(sum(bod.total_juice() for bod in bods)))


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-c", dest="count", action="store", type=int)

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

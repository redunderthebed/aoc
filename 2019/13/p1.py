import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

tile_sets = {i: set() for i in range(1, 5)}

class ArcadeObject(GraphObject):
  def __init__(self, graph, tile_code):
    self.tile_code = tile_code
    super(ArcadeObject, self).__init__(graph)

  def place(self, x, y):
    tile_sets[self.tile_code].add(self)
    super(ArcadeObject, self).place(x,y)

  def remove(self):
    tile_sets[self.tile_code].remove(self)
    super(ArcadeObject, self).remove()

class Wall(ArcadeObject):
  def __init__(self, graph):
    super(Wall, self).__init__(graph, 1)
  def show(self, cell_width):
    return "#" * cell_width

class Block(ArcadeObject):
  def __init__(self, graph):
    super(Block, self).__init__(graph, 2)
  def show(self, cell_width):
    return "B" * cell_width

class Paddle(ArcadeObject):
  def __init__(self, graph):
    super(Paddle, self).__init__(graph, 3)
  def show(self, cell_width):
    return "B" * cell_width

class Ball(ArcadeObject):
  def __init__(self, graph):
    super(Ball, self).__init__(graph, 4)
  def show(self, cell_width):
    return "*" * cell_width

def do_it(args):
  graph = Graph()
  tile_types = {
    0: None,
    1: Wall,
    2: Block,
    3: Paddle,
    4: Ball
  }
  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  for x in args.input or []:
    cpu.load_input(int(x))

  triple = []
  for output in cpu.run():
    triple.append(output)
    if len(triple) == 3:
      tile_type = triple[2]
      tile_pos = triple[0:2]
      if tile_type != 0:
        tile_cls = tile_types[tile_type]
        tile_cls(graph).place(*tile_pos)
        #graph.set_cell(triple[0], triple[1], tile_cls(triple[0], triple[1]))
      else:
        tile = graph.get_cell(*tile_pos)
        if tile is not None:
          tile.remove()
        #graph.free_cell(triple[0], triple[1])
      triple = []
  graph.print_window(0, 0, 40, 20, cell_width=1)
  print(len(tile_sets[2]))

if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

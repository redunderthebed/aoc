import argparse
from itertools import permutations
from graph import Graph
def load_data(filename):
  with open(filename) as file:
    x, y = (0,0)
    for line in file.readlines():
      x = 0
      for bla in line:
        yield (x,y), bla
        x += 1
      y += 1


def do_it(args):
  graph = Graph()
  for coord, val in load_data(args.filename):
    if val == "#":
      graph.set_cell(coord[0], coord[1], val)
  best = (0, None)
  count = 0
  for one in graph.items():
    print("Processing #{}: {}".format(count, one))
    vision_count = 0
    for two in graph.items():
      if one != two:
        blocked = False
        for coord, val in graph.iter_line(*(one[0] + two[0])):
          if val == "#":
            blocked = True
            break
        if not blocked:
          vision_count += 1
    if vision_count > best[0]:
      best = (vision_count, one)
    print("Can see {}".format(vision_count))
  print("Most eye ballin' space ball is at {} and can see {} other space balls".format(best[1], best[0]))

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

import argparse
from itertools import permutations
from graph import Graph
def load_data(filename):
  with open(filename) as file:
    print(filename)
    x, y = (0,0)
    for line in file.readlines():
      x = 0
      for bla in line:
        yield (x,y), bla
        x += 1
      y += 1

def slope(one, two):
  inf = 1000000
  q = None
  if one[0] == two[0]:
    rise = inf if two[1] > one[1] else -inf
    q = (1 if rise == -inf else 5, 0)
    run = 0
    val = None
  elif one[1] == two[1]:
    rise = 0
    run = inf if two[0] > one[0] else -inf
    q = (3 if run == inf else 7, 0)
    val = 0
  else:
    rise = two[1] - one[1]
    run = two[0] - one[0]
    val = ((two[1] - one[1])/(two[0] - one[0]))
  if q is None:
    if rise < 0 and run >= 0:
      q = (2, -abs(val))
    elif rise >= 0 and run >= 0:
      q = (4, abs(val))
    elif run < 0 and rise >= 0:
      q = (6, -abs(val))
    elif run < 0 and rise < 0:
      q = (8, abs(val))
  if q is None:
    raise Exception("no q")
  return val, q

def do_it(args):
  graph = Graph()
  center = None
  for coord, val in load_data(args.filename):
    if val == "#":
      graph.set_cell(coord[0], coord[1], val)
    if val.upper() == "X":
      center = coord

  if center is None:
    #raise Exception ("no center")
    center = (27, 19)

  graph.set_cell(center[0], center[1], "X", force=True)
  count = 0
  target = int(args.limit)
  while count < target:
    open_balls = []
    balls = {}
    for ball, val in graph.items():
      if val != "#":
        continue
      print("Processing #{}: {}".format(count, ball))
      vision_count = 0
      balls[ball] = []

      for coord, val in graph.iter_line(*(center + ball)):
        if val == "#":
          balls[ball].append(coord)

      if len(balls[ball]) == 0:
        open_balls.append((ball, slope(center, ball)[1]))

    open_balls = sorted(open_balls, key=lambda x: x[1])
    print()
    print()
    print(open_balls)
    prev_count = count
    for ball, g in open_balls:
      count += 1
      graph.set_cell(ball[0], ball[1], count, force=True)
      if count == target:
        print("Center: {}".format(center))
        print("ball {} was {}th".format(ball, target))
        break
    if count == prev_count:
      raise Exception("No progress")
  graph.set_cell(*(center + ('x',True)))
  #graph.print_window(0, 0, 25, 10, 2)
  graph.print_window(0, 0, 35, 35, 3)


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-l", dest="limit", action="store", type=int)
  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

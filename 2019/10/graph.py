from math import gcd
class Graph(object):
  def __init__(self):
    self.space = {}

  def set_cell(self, x, y, value, force=False):
    if force or (x,y) not in self.space:
      self.space[(x,y)] = value

  def items(self):
    return self.space.items()

  def free_cell(self, x, y):
    self.space.pop((x,y), None)

  def get_cell(self, x, y):
    return self.space.get((x,y))

  def print_window(self, start_x, start_y, width, height, cell_width=2):
    cell_str="{{:{}}}".format(cell_width)
    print("   ", end="")
    for x in range(start_x, start_x + width):
      if x % (width//5) == 0:
        print("{{:<{}}}".format(cell_width).format(x), end="")
      else:
        print(" "*cell_width, end="")
    print()
    for y in range(start_y, start_y + height):
      if y % (height//3) ==0:
        print("{:3d}".format(y), end="")
      else:
        print("   ", end="")
      for x in range(start_x, start_x + width):
        if (x,y) in self.space:
          print(cell_str.format(self.get_cell(x,y)), end="")
        else:
          print(cell_str.format("."*cell_width), end="")
      print()

  def iter_line(self, x1, y1, x2, y2, inclusive=False):
    run = x2 - x1
    rise = y2 - y1
    d = gcd(rise, run)
    rise_d = 1 if rise > 0 else -1
    run_d = 1 if run > 0 else -1

    if d != 0:
      rise /= d * rise_d
    if d != 0:
      run /= d * run_d
    if int(rise) != rise or int(run) != run:
      raise Exception("I hoped that wouldn't happen")
    run = int(run)
    rise = int(rise)
    x,y = x1,y1
    if rise == 0:
      iter = (((x, y1), self.get_cell(x, y1)) for x in range(x1, x2, run_d))
    elif run == 0:
      iter = (((x1, y), self.get_cell(x1, y)) for y in range(y1, y2, rise_d))
    else:
      run_iter = range(x1, x2 + (run_d if inclusive else 0), run * run_d)
      rise_iter = range(y1, y2 + (rise_d if inclusive else 0), rise * rise_d)
      iter = (((x,y), self.get_cell(x, y)) for x,y in zip(run_iter, rise_iter))

    for thing in iter:
      if inclusive is True:
        yield thing
      if inclusive is False:
        inclusive = True

if __name__ == "__main__":
  g = Graph()

  g.set_cell(12, 3, "#")
  g.set_cell(16, 10, "#")
  g.set_cell(20, 17, "#")
  #g.set_cell(16, 8, "#")
  print(g.space)
  g.print_window(11, 0, 10, 20, cell_width=1)
  for coord, value in g.iter_line(12, 3, 20, 17):
    print(coord, value)

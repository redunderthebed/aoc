import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject
from heapq import *
from math import ceil

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

def do_it(args):
  graph = Graph()
  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  pos = [0,0]
  for output in cpu.run():
    if output == 10:
      pos[1] += 1
      pos[0] = 0
      continue
    if output != 46:
      graph.set_cell(pos[0], pos[1], chr(output))
    pos[0] += 1

  total = 0

  for pos, value in graph.space.items():
    if all([(x is not None and x in "#O") for x in graph.iter_neighbours(*pos)]):
      graph.set_cell(pos[0], pos[1], "O", force=True)
      total += pos[0] * pos[1]
  graph.print_window(-5, -5, 80, 50, cell_width=1)
  print("Total:", total)



if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject
from heapq import *
from math import ceil

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes
class Oxygen(GraphObject):
  bumps = { 0: (0,-1), 1:(0,1), 2:(-1,0), 3:(1,0) }
  most_minutes = 0
  def __init__(self, graph, minutes):
    self.minutes = minutes
    if minutes > Oxygen.most_minutes:
      Oxygen.most_minutes = minutes
    super(Oxygen, self).__init__(graph)

  def move(self, dir):
    print("OXY MOVE", dir)
    return (self.x + self.bumps[dir][0], self.y + self.bumps[dir][1])

  def spread(self):
    children = []
    for i in range(4):
      new_pos = self.move(i)
      neighbour = self.graph.get_cell(*new_pos)
      if isinstance(neighbour, BreadCrumb) or str(neighbour) == "G":
        neighbour.remove()
        o = Oxygen(self.graph, self.minutes + 1)
        o.place(*new_pos)
        children.append(o)
    return children

  def show(self, cell_width):
    return "O" * cell_width

class BreadCrumb(GraphObject):
  bumps = { 0: (0,-1), 1:(0,1), 2:(-1,0), 3:(1,0) }
  def __init__(self, graph, moves):
    self.moves = moves
    self.last_tried = 0
    self.dead = False
    super(BreadCrumb, self).__init__(graph)

  def move(self, dir):
    return (self.x + self.bumps[dir][0], self.y + self.bumps[dir][1])

  def next_move(self):
    neighbours = [self.graph.get_cell(*self.move(i)) for i in range(0, 4)]
    dead_ends = 0
    self.last_tried = None
    unexplored = []
    alive = []
    for i in range(0, 4):
      n = self.graph.get_cell(*self.move(i))
      if n is None:
        unexplored.append(i)
      if isinstance(n, BreadCrumb):
        if n.dead is False:
          alive.append(i)
        else:
          dead_ends += 1
      if str(n) == "#":
        dead_ends += 1
    if dead_ends == 3:
      self.dead = True
    if len(unexplored) != 0:
      self.last_tried = unexplored[0]
    elif len(alive) != 0:
      self.last_tried = alive[0]
    else:
      return None
    return self.last_tried
  def show(self, cell_width):
    return "*" * cell_width

def do_it(args):
  graph = Graph()
  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  cur = BreadCrumb(graph, 0)
  cur.place(0,0)
  goal_pos = None
  move = cur.next_move()
  cpu.load_input(move + 1)
  pos = (0,0)
  for output in cpu.run():
    print(output)
    if output == 0:
      graph.set_cell(*cur.move(move) + ('#',))
    if output >= 1:
      new_pos = cur.move(move)
      occupant = graph.get_cell(*new_pos)
      if isinstance(occupant, BreadCrumb):
        cur = occupant
      else:
        cur = BreadCrumb(graph, cur.moves + 1)
        cur.place(*new_pos)
    if output == 2:
      print("Found the goal at", cur.x, cur.y, "after", cur.moves, "steps")
      goal_pos = (cur.x, cur.y)
    print(cur.moves, "WSAD"[move])
    #graph.print_window(cur.pos[0] - 10, cur.pos[1] - 10, 20, 20, cell_width=5)
    choice = ""
    if len(choice) == 1 and choice.upper() in "WSAD" :
      move = "WSAD".index(choice.upper())
    else:
      if choice == "x":
        import pdb; pdb.set_trace()
      move = cur.next_move()
    if move is not None:
      cpu.load_input(move + 1)
    else:
      break
  oxygens = []
  print("Starting from goal_pos {}".format(goal_pos))
  first = Oxygen(graph, 0)
  first.place(*goal_pos)
  oxygens.append(first)
  while len(oxygens) > 0:
    new_oxygens = []
    for oxygen in oxygens:
      new_oxygens.extend(oxygen.spread())
    print(goal_pos)
    graph.print_window_center(0, 0, 100, 50, 1)
    input(">")
    oxygens = new_oxygens

  print(Oxygen.most_minutes)





if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

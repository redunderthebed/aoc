import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject
from heapq import *
from math import ceil

def load_data(filename):
  with open(filename) as file:
    pos = (0, 0)
    for line in file.readlines():
      for thing in line.replace('\n', ''):
        yield (pos, thing)
        pos = (pos[0] + 1, pos[1])
      pos = (0, pos[1] + 1)

class Action(object):
  def __init__(self, reqs, products, location):
    self.reqs = reqs
    self.products = products
    self.location = location
  def __repr__(self):
    return "{} -> {}".format(self.reqs, self.products)

class ActionNode(object):
  def __init__(self, reqs, action):
    self.reqs = reqs
    self.action = action

  def add_req(self, name, quantity):
    v = self.reqs.get(name, 0)
    self.reqs[name] = v + quantity

  def fulfill(self, name, quantity):
    v = self.reqs.get(name, 0)
    self.reqs[name] = v - quantity

  @property
  def open_reqs(self):
    for k,v in self.reqs.items():
      if v > 0:
        yield (k,v)

  def __lt__(self, other):
    return False

  def __repr__(self):
    return "Action Node: {} reqs after fetching {} @ {}".format(",".join("{}{}".format(k,v) for k,v in self.open_reqs), next((x for x in self.action.products.keys()), None), self.action.location)

def nav_iterate_neighbours(world, held_keys):
  def neighbours(node):
    for pos, value in world.iter_straight_neighbours(*node):
      if value != "#":
        if re.match("[A-Z]", value) and held_keys.get(value.lower(), 1) != 0:
          continue
        yield pos
  return neighbours

def action_iterate_neighbours(keys):
  def neighbours(node):
    for req, qty in node.open_reqs:
      new_node = ActionNode(node.reqs.copy(), Action({}, {req:1}, keys[req]))
      new_node.fulfill(req, 1)
      yield new_node
  return neighbours

def action_check_goal(node):
  # are requirements satisfied?
  # i.e. do I have all the keys
  open = node.open_reqs
  item = next(open, None)
  if item is None:
    return True
  else:
    return False

def action_heuristic(world):
  def heuristic(a, b):
    if b is None:
      return 0
    if b.action.location is None:
      return 1
    data = astar(a.action.location, nav_check_goal(b.action.location), \
            nav_heuristic, nav_iterate_neighbours(world, a.reqs))
    if data:
      return len(data)
    else:
      return 10000000
  return heuristic

def nav_heuristic(a, b):
  if b is None:
    return 0
  return abs(b[0] - a[0]) + abs(b[1] - a[1])

def nav_check_goal(goal):
  def is_goal(a):
    return goal == a
  return is_goal

def astar(start, check_goal, heuristic, neighbours):
  close_set = set()
  came_from = {}
  gscore = {start:0}
  fscore = {start:heuristic(start, None)}
  oheap = []
  heappush(oheap, (fscore[start], start))
  while oheap:
    current = heappop(oheap)[1]
    if check_goal(current):
      data = []
      while current in came_from:
        data.append(current)
        current = came_from[current]
      return data
    close_set.add(current)
    for neighbour in neighbours(current):
      tentative_g_score = gscore[current] + heuristic(current, neighbour)
      if neighbour in close_set and tentative_g_score >= gscore.get(neighbour, 0):
        continue
      if  tentative_g_score < gscore.get(neighbour, 0) \
        or neighbour not in [i[1]for i in oheap]:
        came_from[neighbour] = current
        gscore[neighbour] = tentative_g_score
        fscore[neighbour] = tentative_g_score + heuristic(neighbour, None)
        heappush(oheap, (fscore[neighbour], neighbour))
  return False

def do_it(args):
  graph = Graph()
  cur = None
  gates = {}
  keys = {}
  for pos, value in load_data(args.filename):
    graph.set_cell(*(pos + (value,)))
    if value == "@":
      cur = pos
    if re.match("[A-Z]", value) is not None:
      gates[value] = pos
    if re.match("[a-z]", value) is not None:
      keys[value] = pos
  graph.print_window(0, 0, 25, 10, cell_width=2)
  #graph.print_window_center(cur[0], cur[1], 20, 10, cell_width=2)

  # held_keys = {}
  # data = astar(cur, nav_check_goal((keys['b'])), nav_heuristic, nav_iterate_neighbours(graph, held_keys))
  # if data:
  #   for pos in data:
  #     graph.set_cell(*(pos + ("1",True)))
  #   graph.print_window_center(cur[0], cur[1], 20, 10, cell_width=2)
  #
  #data = astar(cur, nav_check_goal((keys['a'])), nav_heuristic, nav_iterate_neighbours(graph, held_keys))
  # if data:
  #   for pos in data:
  #     graph.set_cell(*(pos + ("2",True)))
  #   graph.print_window_center(cur[0], cur[1], 20, 10, cell_width=2)
  #
  # print('asdf')
  # held_keys = {'a': 0}
  # data = astar(cur, nav_check_goal((keys['a'])), nav_heuristic, nav_iterate_neighbours(graph, held_keys))
  # if data:
  #   for pos in data:
  #     graph.set_cell(*(pos + ("3",True)))
  #   graph.print_window_center(cur[0], cur[1], 20, 10, cell_width=2)
  # print('asdfasdf', data)
  start = ActionNode({k:1 for k in keys}, Action({}, {}, cur))
  h = action_heuristic(graph)
  n = action_iterate_neighbours(keys)
  data = astar(start, action_check_goal, h, n)

  total = 0
  for step in reversed(data):
    print(step.action.location)
    data = astar(cur, nav_check_goal(step.action.location), nav_heuristic, nav_iterate_neighbours(graph, step.reqs))
    total += len(data)
    cur = step.action.location
  print(total)


if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

def load_data():
  with open("../input.txt") as file:
    for line in file.readlines():
      yield int(line)

def calculate_fuel(mass):
  return int((float(mass) / 3.0))-2

def part2():
  total_fuel = 0
  for mass in load_data():
    module_fuel = calculate_fuel(mass)
    fuel_mass = module_fuel
    while True:
      fuel_mass = calculate_fuel(fuel_mass)
      if fuel_mass > 0:
        module_fuel += fuel_mass
      else:
        break
    total_fuel += module_fuel
  print("Total Fuel Required: {}".format(total_fuel))
  return total_fuel

if __name__ == "__main__":
  part2()

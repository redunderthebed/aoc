def load_data():
  with open("../input.txt") as file:
    for line in file.readlines():
      yield int(line)


def calculate_fuel(mass):
  return int((float(mass) / 3.0))-2

def part1():
  total_fuel = 0
  for mass in load_data():
    total_fuel += calculate_fuel(mass)
  print("Total Fuel Required: {}".format(total_fuel))
  return total_fuel
if __name__ == "__main__":
  part1()

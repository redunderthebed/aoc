import argparse
from itertools import permutations
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

class IntCpu(object):
  def __init__(self):
    self.memory = []
    self.input = []
    self.ip = 0
    self.halted = True

    self.ops = {
      1: (self.opcode_add, 3),
      2: (self.opcode_multi, 3),
      3: (self.opcode_input, 1),
      4: (self.opcode_output, 1),
      5: (self.opcode_jump_if_true, 2),
      6: (self.opcode_jump_if_false, 2),
      7: (self.opcode_less_than, 3),
      8: (self.opcode_equals, 3),
      99: (self.opcode_halt, 0)
    }

    self.param_modes =  {
      0: self.param_position,
      1: self.param_immediate
    }

  def load_code(self, filename):
    with open(filename) as file:
      for line in file.readlines():
        codes = line.split(',')
        for code in codes:
          self.memory.append(code)

  def load_input(self, input):
    self.input = [input] + self.input

  def resolve(self, p):
    return int(p[0](p[1]))

  def opcode_add(self, x, y, dest):
    dest = dest[1]
    x, y = self.resolve(x), self.resolve(y)

    result = x + y
    self.memory[dest] = result
    self.ip += 4
    return result

  def opcode_multi(self, x, y, dest):
    dest = dest[1]
    x, y = self.resolve(x), self.resolve(y)

    result = x * y
    self.memory[dest] = result
    self.ip += 4
    return result

  def opcode_halt(self):
    self.halted = True

  def opcode_input(self, dest):
    dest = dest[1]
    value = self.input.pop()
    self.memory[dest] = int(value)
    self.ip += 2
    return value

  def opcode_output(self, src):
    src = self.resolve(src)
    self.ip += 2
    return src

  def opcode_jump_if_true(self, cond, target):
    cond = self.resolve(cond)
    target = self.resolve(target)

    if cond != 0:
      self.ip = target
      return target
    else:
      self.ip += 3
      return None

  def opcode_jump_if_false(self, cond, target):
    cond = self.resolve(cond)
    target = self.resolve(target)

    if cond == 0:
      self.ip = target
      return target
    else:
      self.ip += 3
      return None

  def opcode_less_than(self, left, right, dest):
    left = self.resolve(left)
    right = self.resolve(right)
    dest = dest[1]
    result = 1 if left < right else 0
    self.memory[dest] = result
    self.ip += 4
    return result

  def opcode_equals(self, left, right, dest):
    left = self.resolve(left)
    right = self.resolve(right)
    dest = dest[1]
    result = 1 if left == right else 0
    self.memory[dest] = result
    self.ip += 4
    return result

  def param_position(self, value):
    return self.memory[value]

  def param_immediate(self, value):
    return value

  def run(self):
    self.halted = False
    while self.halted is False and self.ip < len(self.memory):
      opcode_whole = str( self.memory[self.ip] )
      mode_part = opcode_whole[:-2]
      opcode = int(opcode_whole[-2:])

      op, p_count = self.ops[opcode]
      mode_part = (("0" * p_count) + mode_part)[-p_count:]
      params = self.memory[self.ip + 1: self.ip + p_count + 1]

      for i in range(len(params)):
        mode = int(mode_part[-(i+1)])
        p = (self.param_modes[mode], int(params[i]))
        params[i] = p

      s = "op: {} -- {}".format(
        op.__name__.replace("opcode_", "").upper(),
        ["{} ({})".format(param[1], self.resolve(param)) for param in params]
      )

      output = op(*params)

      s += " : " + str(output)
      #print(s)
      if op == self.opcode_output:
        #print(">>", output)
        yield output

def do_it(args):
  max_output = (0, [])
  for perm in permutations([5,6,7,8,9], 5):
    try:
      thrusters = []
      for i in perm:
        cpu = IntCpu()
        cpu.load_code(args.filename)
        cpu.load_input(i)
        thrusters.append([cpu, None])
      #print(thrusters[0][1])
      output = 0
      while all([x[1] is None or x[0].halted is False for x in thrusters]):

        for cpu in thrusters:
          cpu[0].load_input(output)
          #print('input', cpu[0].input)
          if cpu[1] is None:
            cpu[1] = cpu[0].run()
          new_output = next(cpu[1], None)
          if cpu[0].halted is True:
            break
          else:
            output = new_output
          #print(output)

      if output is not None and output > max_output[0]:
        max_output = (output, perm)

      print("Output: {}".format(output))
    except Exception as e:
      print("Failed because", e)
  print("Max Output: {} Sequence: {}".format(*max_output))

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

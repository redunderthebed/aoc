def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      for num in line.split('-'):
        yield int(num)

def part1():
  low, high = load_data()
  count = 0
  for num in range(low, high):
    num_str = str(num)
    conds = [False, True]
    for i in range(len(num_str) - 1):
      if num_str[i] == num_str[i + 1]:
        conds[0] = True
      if num_str[i] > num_str[i + 1]:
        conds[1] = False
    if all(conds):
      count += 1
  print(count)
if __name__ == "__main__":
  part1()

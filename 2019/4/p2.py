def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      for num in line.split('-'):
        yield int(num)

def part2():
  low, high = load_data()
  count = 0
  for num in range(low, high):
    num_str = str(num)
    conds = [(None, 0), True]
    for i in range(len(num_str)):
      if conds[0] is not True:
        if conds[0][0] == num_str[i]:
          conds[0] = (num_str[i], conds[0][1] + 1)
        else: # conds[0][0] != num_str[i]:
          if conds[0][1] == 2:
            conds[0] = True
          else:
            conds[0] = (num_str[i], 1)
      if i > 0 and num_str[i-1] > num_str[i]:
        conds[1] = False
    conds[0] = conds[0] == True or conds[0][1] == 2
    if all(conds):
      count += 1
  print(count)
if __name__ == "__main__":
  part2()

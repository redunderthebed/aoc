import argparse
from itertools import permutations
from intcode import IntCpu
from graph import Graph

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

def turn_right(facing):
  return (-facing[1], facing[0])

def turn_left(facing):
  return (facing[1], -facing[0])

def do_it(args):
  graph = Graph()
  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  for x in args.input or []:
    cpu.load_input(int(x))

  robo_pos = (0,0)
  robo_face = (0,-1)

  cpu.load_input(1)
  output_count = 0
  for output in cpu.run():
    output_count += 1
    if output_count % 2 == 1:
      new_color = output
      graph.set_cell(*(robo_pos + (new_color,True)))
    else:
      turn = output
      if turn == 0:
        robo_face = turn_left(robo_face)
      elif turn == 1:
        robo_face = turn_right(robo_face)
      else:
        raise Exception("Invalid output: {}".format(output))
      robo_pos = (robo_pos[0] + robo_face[0], robo_pos[1] + robo_face[1])
      color = graph.get_cell(*robo_pos) or 0
      cpu.load_input(color)

  print(len(graph.space))
  graph.print_window(0, 0, 50, 10, cell_width=1, mask=[0])
if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")
  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      codes = line.split(',')
      for code in codes:
        yield code

def opcode_add(memory, x, y, result):
  memory[result] = memory[x] + memory[y]
  return 4

def opcode_multi(memory, x, y, result):
  memory[result] = memory[x] * memory[y]
  return 4
def opcode_halt(memory, x=None, y=None, result=None):
  return False

def part1():
  for verb in range(100):
    for noun in range(100):
      i = 0
      memory = [int(x) for x in load_data()]
      memory[1] = noun
      memory[2] = verb

      ops = {
        1: opcode_add,
        2: opcode_multi,
        99: opcode_halt
      }
      while i < len(memory):
        opcode = memory[i]
        op = ops[opcode]
        result = op(memory, *memory[i+1:i+4])
        if result is False:
          break
        else:
          i += result
      if memory[0] == 19690720:
        print("Verb: {} Noun: {}, answer: {}".format(verb, noun, 100*noun+verb))
        return
if __name__ == "__main__":
  part1()

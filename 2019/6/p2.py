import sys
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

def iter_ancestors(orbits, child, target="COM"):
  x = child
  count = 0
  while x != target:
    count += 1
    x = orbits[x]
    yield (x, count)

def do_it(filename):
  orbits = {}
  for center, orbit in load_data(filename):
    if orbit in orbits:
      raise Exception("Oh no!")
    orbits[orbit] = center

  daddies = {}
  for daddy in iter_ancestors(orbits, "YOU"):
    daddies[daddy[0]] = daddy[1]

  common_daddy = None
  for santa_daddy, count in iter_ancestors(orbits, "SAN"):
    if santa_daddy in daddies:
      print("common daddy: {} - {}".format(santa_daddy, count))
      common_daddy = santa_daddy
      break
  from_santa = list(iter_ancestors(orbits, "SAN", common_daddy))[-1]
  from_you   = list(iter_ancestors(orbits, "YOU", common_daddy))[-1]
  print("Santa Transfers: {} You Transfers {} -- {}".format(from_santa, from_you, from_santa[1] + from_you[1] - 2))


if __name__ == "__main__":
  args = sys.argv[1:]
  filename = args[0] if len(args) > 0 else "input.txt"
  do_it(filename)

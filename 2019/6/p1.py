import sys
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

def do_it(filename):
  orbits = {}
  total_indirect = 0
  total_direct = 0
  for center, orbit in load_data(filename):
    orbits[orbit] = center
  for orbit in orbits:
    x = orbits[orbit]
    direct = 0
    indirect = 0
    while x != "COM":
      x = orbits[x]
      indirect += 1
    total_indirect += indirect
    total_direct += 1

  print("Direct {} Indirect {} = {}".format(total_direct, total_indirect, total_direct + total_indirect))


if __name__ == "__main__":
  args = sys.argv[1:]
  filename = args[0] if len(args) > 0 else "input.txt"
  do_it(filename)

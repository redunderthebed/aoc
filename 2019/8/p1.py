import argparse
from collections import Counter
def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      for c in line:
        if c != '\n':
          yield int(c)

def do_it(args):
  data = load_data(args.filename)
  width, height = (25, 6)

  layers = []
  best_layer = (width*height, None)
  try:
    while True:
      layer = {}
      counter = Counter()
      for c in range(width):
        for r in range(height):
          layer[(c,r)] = next(data)
      counter.update(layer.values())
      print(counter)
      if counter[0] < best_layer[0]:
        best_layer = (counter[0], layer)


  except StopIteration as si:
    print("Done")

  print(best_layer)
  counter = Counter()
  counter.update(best_layer[1].values())
  print(counter[1] * counter[2])

if __name__ == "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

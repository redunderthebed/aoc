import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject
from heapq import *
from math import ceil

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      reagent_parts, product_parts = re.split('=>', line.replace('\n', ''))
      reagents = []
      products = []
      for parts, collection in ((reagent_parts, reagents), (product_parts, products)):
        for bit in re.split(', ', parts.strip()):
          print(bit, bit.split(' '))
          quant, name = bit.split(' ')
          collection.append({'name': name, 'quantity': int(quant)})

      yield {'reagents': reagents, 'products': products}

def do_it(args):
  product_lookup = {}
  for reaction in load_data(args.filename):
    print("Reaction", reaction)
    for product in reaction.get('products'):
      product_lookup[product['name']] = reaction
  goal = product_lookup["FUEL"]
  print("Goal:", goal)
  start = ChemNode({}, None, 0)
  start = start.next(goal, 1)
  print(start)
  data = astar(start, check_goal, heuristic, chem_neighbours(product_lookup))
  print()
  for node in data:
    print(node)
    print(node.multiplicity, node.reqs)
  print("Required Ore", data[0].reqs['ORE'])

class ChemNode(object):
  def __init__(self, reqs, reaction, multi):
    self.reqs = reqs
    self.reaction = reaction
    self.multiplicity = multi

  def add_req(self, name, quantity):
    v = self.reqs.get(name, 0)
    self.reqs[name] = v + quantity

  def fulfill(self, name, quantity):
    v = self.reqs.get(name, 0)
    self.reqs[name] = v - quantity

  def next(self, reaction, multiplicity):
    new_node = ChemNode(self.reqs.copy(), reaction, multiplicity)
    for reagent in reaction.get('reagents'):
      new_node.add_req(reagent.get('name'), reagent.get('quantity')*multiplicity)
    for product in reaction.get('products'):
      new_node.fulfill(product.get("name"), product.get("quantity")*multiplicity)
    return new_node

  @property
  def open_reqs(self):
    for k,v in self.reqs.items():
      if v > 0:
        yield (k,v)

  def __lt__(self, other):
    return False

  def __repr__(self):
    return "ChemNode: {} => {}".format(
      ["{} {}".format(x['quantity'],x['name']) for x in self.reaction.get('reagents')],
      ["{} {}".format(x['quantity'],x['name']) for x in self.reaction.get('products')]
    )

def chem_neighbours(reactions):
  def neighbours(node):
    for req_name, req_qty in node.open_reqs:
      reaction = reactions.get(req_name, None)
      if reaction is not None:
        desired = [x for x in reaction.get('products') if x['name'] == req_name]
        if len(desired) == 1:
          desired = desired[0]
          m = ceil(req_qty / desired['quantity'])
          next_node = node.next(reaction, m)
          yield next_node
  return neighbours

def check_goal(node):
  open = node.open_reqs
  item = next(open, None)
  if item is None or item[0] != "ORE":
    return False
  if next(open, None) is not None:
    return False
  print("goal reached", node)
  return True

def heuristic(a, b):
  a_points = 0
  b_points = 0
  for req, qty in a.reqs.items():
    a_points += 5 + qty
  if b is None:
    return a_points
  for req, qty in b.reqs.items():
    b_points += 5 + qty
  return a_points - b_points

def astar(start, check_goal, heuristic, neighbours):

    close_set = set()
    came_from = {}
    gscore = {start:0}
    fscore = {start:heuristic(start, None)}
    oheap = []

    heappush(oheap, (fscore[start], start))

    while oheap:

        current = heappop(oheap)[1]

        if check_goal(current):
            data = []
            while current in came_from:
                data.append(current)
                current = came_from[current]
            return data

        close_set.add(current)
        for neighbour in neighbours(current):
            tentative_g_score = gscore[current] + heuristic(current, neighbour)

            if neighbour in close_set and tentative_g_score >= gscore.get(neighbour, 0):
                continue

            if  tentative_g_score < gscore.get(neighbour, 0) or neighbour not in [i[1]for i in oheap]:
                came_from[neighbour] = current
                gscore[neighbour] = tentative_g_score
                fscore[neighbour] = tentative_g_score + heuristic(neighbour, None)
                heappush(oheap, (fscore[neighbour], neighbour))

    return False



if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

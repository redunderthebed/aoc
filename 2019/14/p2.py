import argparse
import re
from itertools import combinations
from intcode import IntCpu
from graph import Graph, GraphObject

def load_data(filename):
  with open(filename) as file:
    for line in file.readlines():
      codes = line.replace('\n', '').split(')')
      yield codes

tile_sets = {i: set() for i in range(1, 5)}

class ArcadeObject(GraphObject):
  def __init__(self, graph, tile_code):
    self.tile_code = tile_code
    super(ArcadeObject, self).__init__(graph)

  def place(self, x, y):
    tile_sets[self.tile_code].add(self)
    super(ArcadeObject, self).place(x,y)

  def remove(self):
    tile_sets[self.tile_code].remove(self)
    super(ArcadeObject, self).remove()

class Wall(ArcadeObject):
  def __init__(self, graph):
    super(Wall, self).__init__(graph, 1)
  def show(self, cell_width):
    return "#" * cell_width

class Block(ArcadeObject):
  def __init__(self, graph):
    super(Block, self).__init__(graph, 2)
  def show(self, cell_width):
    return "B" * cell_width

class Paddle(ArcadeObject):
  def __init__(self, graph):
    super(Paddle, self).__init__(graph, 3)
  def show(self, cell_width):
    return "P" * cell_width

class Ball(ArcadeObject):
  def __init__(self, graph):
    self.pos_from = None
    self.dir = None
    super(Ball, self).__init__(graph, 4)

  def going(self):
    cur = self.pos
    while True:
      obstacle = self.graph.get_cell(*cur)
      if isinstance(obstacle, (Wall, Block)) or cur[1] >= 18:
        break
      cur = (cur[0] + self.dir[0], cur[1] + self.dir[1])
    return cur

  def show(self, cell_width):
    return "*" * cell_width
  def place(self, x, y):
    if self.pos_from is not None:
      self.dir = (x - self.pos_from[0], y - self.pos_from[1])
    else:
      self.pos_from = (x,y)
      self.dir = (1,1)
    super(Ball, self).place(x, y)
  def remove(self):
    self.pos_from = (self.x, self.y)
    super(Ball, self).remove()

class Brain(object):
  def __init__(self):
    self.ball = None
    self.paddle = None
    self.tile_sets = tile_sets

  def ctrl_paddle(self):
    if self.ball is not None and self.paddle is not None:
      if self.ball.going()[0] > self.paddle.x:
        move = 1
      elif self.ball.going()[0] < self.paddle.x:
        move = -1
      else:
        move = 0
      return move

def do_it(args):
  graph = Graph()
  tile_types = {
    0: None,
    1: Wall,
    2: Block,
    3: Paddle,
    4: Ball
  }
  cpu = IntCpu(args.verbose, args.debug)
  cpu.load_code(args.filename)
  for x in args.input or []:
    cpu.load_input(int(x))
  cpu.memory[0] = 2

  def joy_stick():
    graph.print_window(0, 0, 40, 20, cell_width=1)
    val = input("A S D")
    if val == "":
      return 0
    val = "asd".index(val.lower())
    val = (val - 1)
    return val
  brain = Brain()
  cpu.set_input_callback(brain.ctrl_paddle)
  triple = []
  for output in cpu.run():
    triple.append(output)
    if len(triple) == 3:
      tile_type = triple[2]
      tile_pos = triple[0:2]
      if tile_pos[0] == -1 and tile_pos[1] == 0:
        print('new score {}'.format(tile_type))
      elif tile_type != 0:
        tile_cls = tile_types[tile_type]
        if tile_cls == Ball:
          tile = brain.ball or tile_cls(graph)
          brain.ball = tile
        elif tile_cls == Paddle:
          tile = brain.paddle or tile_cls(graph)
          brain.paddle = tile
        else:
          tile = tile_cls(graph)
        tile.place(*tile_pos)
        #graph.set_cell(triple[0], triple[1], tile_cls(triple[0], triple[1]))
      else:
        tile = graph.get_cell(*tile_pos)
        if tile is not None:
          tile.remove()
        #graph.free_cell(triple[0], triple[1])
      triple = []
  graph.print_window(0, 0, 40, 20, cell_width=1)

if __name__ ==   "__main__":
  parser = argparse.ArgumentParser("Pointless elf shit!")
  parser.add_argument("-f", dest="filename", action="store")
  parser.add_argument("-fn", dest="filecat", action="store")
  parser.add_argument("-d", dest="debug", action="store_true", default=False)
  parser.add_argument("-v", dest="verbose", action="store_true", default=False)
  parser.add_argument("-i", nargs="+", dest="input", action="store")

  args = parser.parse_args()
  if (args.filecat or '').startswith('i'):
    args.filename = "input" + args.filecat[1:] + ".txt"
  if (args.filecat or '').startswith('e'):
    args.filename = "example" + args.filecat[1:] + ".txt"
  if args.filename is None:
    args.filename = "input.txt"
  do_it(args)

def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      yield line

def load_sample_data():
  samples = [
    ["R75,D30,R83,U83,L12,D49,R71,U7,L72",
    "U62,R66,U55,R34,D71,R55,D58,R83"],
    ["R8,U5,L5,D3","U7,R6,D4,L4"]
  ]
  for line in samples[1]:
    yield line
def part2():
  space = {}
  dirs = {
    'R': (1, 0),
    'U': (0, -1),
    'L': (-1, 0),
    'D': (0, 1)
  }
  closest = None
  lead_idx = 0
  for lead in load_data():
    lead_idx += 1
    pos = (0, 0)
    steps = 0
    for code in lead.split(','):
      dir, mag = code[0], int(code[1:])
      vec = dirs[dir]
      for i in range(mag):
        steps += 1
        pos = (pos[0] + (1*vec[0]), pos[1] + (1*vec[1]))
        occ = space.get(pos, None)
        if occ is None:
          space[pos] = (lead_idx, steps)
        elif occ[0] == lead_idx:
          pass
        else:
          dist = steps + occ[1]
          if closest is None or dist < closest:
            closest = dist

  print("Closest collision is at step distance {}".format(closest))

if __name__ == "__main__":
  part2()

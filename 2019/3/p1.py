def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      yield line

def part1():
  space = {}
  dirs = {
    'R': (1, 0),
    'U': (0, -1),
    'L': (-1, 0),
    'D': (0, 1)
  }
  closest = None
  lead_idx = 0
  for lead in load_data():
    lead_idx += 1
    pos = (0, 0)
    for code in lead.split(','):
      dir, mag = code[0], int(code[1:])
      vec = dirs[dir]
      for i in range(mag):
        pos = (pos[0] + (1*vec[0]), pos[1] + (1*vec[1]))
        occ = space.get(pos, None)
        if occ is None:
          space[pos] = lead_idx
        elif occ == lead_idx:
          pass
        elif closest is None or sum([abs(x) for x in closest]) > sum([abs(x) for x in pos]):
          closest = pos
  print("Closest collision is at {}, Manhattan distance: {}".format(closest, sum(closest)))





if __name__ == "__main__":
  part1()

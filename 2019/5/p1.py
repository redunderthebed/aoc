from functools import partial
memory = []
def load_data():
  with open("input.txt") as file:
    for line in file.readlines():
      codes = line.split(',')
      for code in codes:
        yield code

def resolve(p):
  return p[0](p[1])

def opcode_add(i, input, x, y, result):
  global memory
  result = result[1]
  x, y = resolve(x), resolve(y)
  memory[result] = x + y
  return (i + 4,)

def opcode_multi(i, input, x, y, result):
  global memory
  result = result[1]
  x, y = resolve(x), resolve(y)
  memory[result] = x * y
  return (i + 4,)

def opcode_halt(i, input):
  return (False,)

def opcode_input(i, input, dest):
  global memory
  dest = dest[1]
  value = input.pop()
  memory[dest] = int(value)
  print("input", dest, value, memory[dest])
  return (i + 2,)

def opcode_output(i, input, src):
  global memory
  src = resolve(src)
  return (i + 2, src)

def param_position(value):
  global memory
  return memory[value]

def param_immediate(value):
  global memory
  return value

def part1():
  ip = 0
  global memory
  memory.extend([int(x) for x in load_data()])

  ops = {
    1: (opcode_add, 3),
    2: (opcode_multi, 3),
    3: (opcode_input, 1),
    4: (opcode_output, 1),
    99: (opcode_halt, 1)
  }

  param_modes =  {
    0: param_position,
    1: param_immediate
  }

  input = ["1"]
  traceback = []
  while ip < len(memory):
    opcode_whole = str(memory[ip])
    mode_part = opcode_whole[:-2]
    opcode = int(opcode_whole[-2:])

    op, p_count = ops[opcode]
    mode_part = (("0" * p_count) + mode_part)[-p_count:]
    params = memory[ip + 1:ip + p_count + 1]
    print("params", params)

    for i in range(len(params)):
      mode = int(mode_part[-(i+1)])
      p = (param_modes[mode], int(params[i]))
      params[i] = p

    traceback.append((ip, opcode_whole, [(x[1], x[0](x[1])) for x in params]))
    result = op(ip, input, *params)
    if result[0] is False:
      break
    else:
      ip = result[0]
      if len(result) > 1:
        print(">>", result[1])
        if result[1] != 0:
          for line in traceback:
            ip, code, params = line
            print("{:3}: Opcode: {:5} Params: {}".format(ip, code, [ "{} ({})".format(x[0], x[1]) for x in params]))
          break

if __name__ == "__main__":
  part1()

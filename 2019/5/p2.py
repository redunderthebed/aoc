from functools import partial
import sys

def load_data():
  with open("input2.txt") as file:
    for line in file.readlines():
      codes = line.split(',')
      for code in codes:
        yield code

def resolve(p):
  return p[0](p[1])

def opcode_add(memory, i, input, x, y, result):
  result = result[1]
  x, y = resolve(x), resolve(y)
  memory[result] = x + y
  return (i + 4,)

def opcode_multi(memory, i, input, x, y, result):
  result = result[1]
  x, y = resolve(x), resolve(y)
  memory[result] = x * y
  return (i + 4,)

def opcode_halt(memory, i, input):
  return (False,)

def opcode_input(memory, i, input, dest):
  dest = dest[1]
  value = input.pop()
  memory[dest] = int(value)
  return (i + 2,)

def opcode_output(memory, i, input, src):
  src = resolve(src)
  return (i + 2, src)

def opcode_jump_if_true(memory, i, input, cond, target):
  cond = resolve(cond)
  target = resolve(target)
  print("JIT", cond, target)
  if cond != 0:
    return (target,)
  else:
    return (i + 3,)

def opcode_jump_if_false(memory, i, input, cond, target):
  cond = resolve(cond)
  target = resolve(target)
  if cond == 0:
    return (target,)
  else:
    return (i + 3,)

def opcode_less_than(memory, i, input, left, right, dest):
  left = resolve(left)
  right = resolve(right)
  dest = dest[1]
  memory[dest] = 1 if left < right else 0
  return (i + 4,)

def opcode_equals(memory, i, input, left, right, dest):
  left = resolve(left)
  right = resolve(right)
  dest = dest[1]
  memory[dest] = 1 if left == right else 0
  return (i+4,)

def param_position(memory, value):
  return memory[value]

def param_immediate(meory, value):
  return value

def print_traceback(ops, traceback):
  for line in traceback:
    ip, mode_part, code, params = line
    print("{:3}: Modes: {:5} - Opcode: {:5} -- Params: {}".format(
      ip,
      "".join(reversed(mode_part)), ops[int(code)][0].__name__.replace("opcode_", "(" + str(code) + ") "),
      [ "{} ({})".format(x[0], x[1]) for x in params]
    ))

def part2(input_buffer):
  ip = 0
  memory = []
  memory.extend([int(x) for x in load_data()])

  ops = {
    1: (opcode_add, 3),
    2: (opcode_multi, 3),
    3: (opcode_input, 1),
    4: (opcode_output, 1),
    5: (opcode_jump_if_true, 2),
    6: (opcode_jump_if_false, 2),
    7: (opcode_less_than, 3),
    8: (opcode_equals, 3),
    99: (opcode_halt, 0)
  }

  param_modes =  {
    0: param_position,
    1: param_immediate
  }

  input = input_buffer
  traceback = []
  exit_ok = False
  try:
    while ip < len(memory):
      opcode_whole = str(memory[ip])
      mode_part = opcode_whole[:-2]
      opcode = int(opcode_whole[-2:])

      op, p_count = ops[opcode]
      mode_part = (("0" * p_count) + mode_part)[-p_count:]
      params = memory[ip + 1:ip + p_count + 1]

      for i in range(len(params)):
        mode = int(mode_part[-(i+1)])
        p = (partial(param_modes[mode], memory), int(params[i]))
        params[i] = p

      traceback.append((ip, mode_part, opcode, [(x[1], x[0](x[1])) for x in params]))
      result = op(memory, ip, input, *params)
      if result[0] is False:
        exit_ok = True
        break
      else:
        ip = result[0]
        if len(result) > 1:
          print(">>", result[1])
          if result[1] != 0:
            print_traceback(ops, traceback)
            break
  except Exception as e:
    print(e)
  if exit_ok is False:
    print_traceback(ops, traceback)

if __name__ == "__main__":
  input_buffer = sys.argv[1:]
  part2(input_buffer)

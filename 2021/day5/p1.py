import common
import re


def fetch_lines():
    for line in common.read_input(5, 1, input_name=None):
        x1, y1, x2, y2 = [int(x) for x in re.match(r"(?P<x1>\d+),(?P<y1>\d+) -> (?P<x2>\d+),(?P<y2>\d+)", line).groups()]
        start = (x1, y1)
        end = (x2, y2)
        yield start, end


def filter_right_angles(lines):
    for start, end in lines:
        if start[0] == end[0] or start[1] == end[1]:
            yield start, end


def right_unit_vector(vector):
    unit = []
    for i in range(len(vector)):
        if vector[i] > 0:
            unit.append(1)
        elif vector[i] < 0:
            unit.append(-1)
        else:
            unit.append(0)
    return tuple(unit)


def distance(vector):
    for i in range(len(vector)):
        if vector[i] != 0:
            return vector[i]


def draw_line(space, pos):
    value = space.get(*pos, value=0)
    space.set(value + 1, *pos)


def do(day, part):
    space = common.Space(n_dims = 2)
    lines = fetch_lines()
    if part == 1:
        lines = filter_right_angles(lines)
    for start, end in lines:
        direction = (end[0] - start[0], end[1] - start[1])
        unit = right_unit_vector(direction)
        pos = start
        draw_line(space, pos)
        print(start, end, direction, unit, distance(direction))
        for i in range(abs(distance(direction))):
            pos = space._move(pos, unit, 1)
            draw_line(space, pos)
    print(space.view_map(empty_value="."))
    hot_spots = sum(1 for pos, value in space.iterate_map() if value is not None and value >= 2)
    return hot_spots

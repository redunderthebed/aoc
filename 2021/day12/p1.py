import common
import re
import enum


class CaveSize(enum.Enum):
    BIG = 1
    SMALL = 0


def quantum_filter_visited(path, node, value, visited):
    if node in ['start', 'end']:
        return True

    if value['size'] == CaveSize.SMALL:
        if not set(visited.values()).intersection(set([-1, 1])):
            raise common.QuantumVisit(1, 0)
        elif visited.get(node, 0) != 0:
            return visited.get(node, 0)
        else:
            return True


def filter_visited(path, node, value, visited):
    return value['size'] == CaveSize.SMALL


def do(day, part):
    net = common.Network()
    for line in common.read_input(day, part, "example"):
        _from, _to = re.match("(\w+)-(\w+)", line).groups()
        if not net.has_node(_from):
            net.set_node_value(_from, {'size': CaveSize.BIG if _from.isupper() else CaveSize.SMALL})
        if not net.has_node(_to):
            net.set_node_value(_to, {'size': CaveSize.BIG if _to.isupper() else CaveSize.SMALL})
        net.add_connection(_from, _to, True)

    nq = common.NetworkQuery(visited_filter=filter_visited if part == 1 else quantum_filter_visited)
    paths = set(nq.depth_first_paths(net, "start", "end"))
    paths = sorted([tuple(x[0] for x in path) + (path[-1][1],) for path in paths])
    for path in paths:
        print(path)

    return len(paths)



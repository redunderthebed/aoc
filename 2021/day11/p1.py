import common


class Octodance(common.Space):

    def __init__(self, dimensions=None, diminsions=None, n_dims=2, bounded=False):
        self.total_flashes = 0
        self.flashed_positions = set()
        super(Octodance, self).__init__(dimensions, diminsions, n_dims, bounded)

    def incr_pus(self, pos):
        value = self.get(*pos)
        self.set(value + 1, *pos)
        return value + 1 > 9

    def dance_step(self):
        self.flashed_positions = set()
        for pos, value in self.iterate_map():
            if value is not None and self.incr_pus(pos):
                self.flash(pos)

        if len(self.flashed_positions) == len(self.space):
            return True

        for pos in self.flashed_positions:
            self.set(0, *pos)
        return False

    def flash(self, flashpoint):
        self.set(0, *flashpoint)
        self.total_flashes += 1
        self.flashed_positions.add(flashpoint)
        for pos, value in self.iterate_neighbours(flashpoint, 1):
            if value is not None:
                if pos != flashpoint:
                    self.incr_pus(pos)
                if value+1 > 9 and pos not in self.flashed_positions:
                    self.flash(pos)


def do(day, part):
    space = Octodance(n_dims=2)

    for line in common.read_input(day, part):
        space.append(line, clean=int)

    if part == 1:
        for i in range(1, 101):
            space.dance_step()
        print(space.view_map())
        return space.total_flashes
    else:
        all_flashed = False
        step = 0
        while not all_flashed:
            step += 1
            all_flashed = space.dance_step()
        return step







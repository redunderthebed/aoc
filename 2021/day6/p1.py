import common

def do(day, part):
    stages = {i: 0 for i in range(9)}
    for x in next(common.read_input(day, part)).split(','):
        stages[int(x)] += 1
    for turn in range(80 if part == 1 else 256):
        print(stages)
        new_stages = {i: 0 for i in range(9)}
        for stage, value in stages.items():
            if stage == 0:
                new_stages[6] += value
                new_stages[8] += value
            else:
                new_stages[stage - 1] += value
        stages = new_stages
    return sum(stages.values())
import unittest
import common

class TestNetwork(unittest.TestCase):
    def test_add_connection(self):
        net = common.Network()
        net.add_connection("A", "B", True)
        self.assertEqual(net.get_connections("A"), {'B'})
        self.assertEqual(net.get_connections("B"), {'A'})

    def test_get_connections__no_node(self):
        net = common.Network()
        self.assertEqual(net.get_connections("A"), set())

    def test_has_node(self):
        net = common.Network()
        net.add_connection("A", "B", False)
        self.assertTrue(net.has_node("A"))
        self.assertTrue(net.has_node("B"))
        self.assertFalse(net.has_node("C"))

    def test_set_node_value(self):
        net = common.Network()
        net.set_node_value("A", 6)
        net.add_connection("A", "B")
        self.assertEqual(net.get_node_value("A"), 6)


class TestNetworkQuery(unittest.TestCase):
    def test_depth_first_paths(self):
        net = common.Network()
        net.add_connection("A", "B")
        net.add_connection("B", "D")
        net.add_connection("A", "C")
        net.add_connection("C", "D")

        paths = set()

        nq = common.NetworkQuery()
        paths = nq.depth_first_paths(net, "A", "D")
        self.assertSetEqual(set(paths), {(("A", "B"), ("B", "D")), (("A", "C"), ("C", "D"))})

    def test_depth_first_paths_looped(self):
        net = common.Network()
        # A -- B -- C -- D
        #     | |
        #      E
        net.add_connection("A", "B")
        net.add_connection("B", "C")
        net.add_connection("C", "D")
        net.add_connection("B", "E", bidirectional=True)

        nq = common.NetworkQuery(visited_filter=lambda path, node, value, visited: False)
        paths = nq.depth_first_paths(net, "A", "D")
        expected = {
            (("A", "B"), ("B", "C"), ("C", "D")),
            (("A", "B"), ("B", "E"), ("E", "B"), ("B", "C"), ("C", "D"))
        }
        print(paths)
        self.assertSetEqual(set(paths), expected)
        nq.visited_filter = lambda path, node, value, visited: node == "B"
        paths = nq.depth_first_paths(net, "A", "D")
        self.assertSetEqual(set(paths), {(("A", "B"), ("B", "C"), ("C", "D"))})

    def test_depth_first_looped_revisits_allowed(self):
        pass

    def test_depth_first(self):
        net = common.Network()
        net.add_connection("A", "B")
        net.add_connection("B", "D")
        net.add_connection("A", "C")
        net.add_connection("C", "E")

        nq = common.NetworkQuery()
        nodes = nq.depth_first_nodes(net, "A")
        self.assertEqual(nodes[0], "A")
        self.assertEqual(nodes[nodes.index("C") + 1], "E")
        self.assertEqual(nodes[nodes.index("B") + 1], "D")

    def test_depth_first_looped(self):
        net = common.Network()
        net.add_connection("A", "B")
        net.add_connection("B", "C")
        net.add_connection("C", "A")

        nq = common.NetworkQuery()
        nodes = nq.depth_first_nodes(net, "A")

        expected = ["A", "B", "C"]
        self.assertEqual(len(expected), len(nodes))
        self.assertEqual(set(expected), set(nodes))

import common
import math
import operator

class PacketDecoder(object):
    def __init__(self, data):
        self.unread = data
        self.read_data = ""
        self.bits_read = 0
        self._version = None
        self._packet_type = None
        self._literal = None
        self._length_type = None
        self._length = None
        self._sub_packets = None
        self._literals = []
        self._op_value = None
        self.version_count = 0

    def read(self, count):
        output = self.unread[:count]
        self.unread = self.unread[count:]
        self.read_data = self.read_data + output
        self.bits_read += count
        return output

    def read_version(self):
        if self._version is None:
            assert self.bits_read == 0
            self._version = int(self.read(3), 2)

    def read_packet_type(self):
        if self._packet_type is None:
            assert self.bits_read == 3
            self._packet_type = int(self.read(3), 2)

    def read_literal(self):
        if self._literal is None:
            assert self._packet_type == 4
            bits = None
            total = ""
            while not bits or bits[0] == "1":
                bits = self.read(5)
                total += bits[1:]
            self._literal = int(total, 2)
            # if self.bits_read % 4 != 0:
            #     self.read(4 - self.bits_read % 4)

    def read_length_type(self):
        if self._length_type is None:
            self._length_type = int(self.read(1), 2)

    def read_length(self):
        if self._length is None:
            if self.length_type == 0:
                self._length = int(self.read(15), 2)
            else:
                self._length = int(self.read(11), 2)

    @property
    def version(self):
        return self._version

    @property
    def packet_type(self):
        return self._packet_type

    @property
    def literal(self):
        return self._literal

    @property
    def length_type(self):
        return self._length_type

    @property
    def length(self):
        return self._length

    @property
    def literals(self):
        return self._literals

    @property
    def op_value(self):
        return self._op_value

    def do_operation(self):
        if self.packet_type in range(0, 4):
            self._op_value = [sum, math.prod, min, max][self.packet_type](self.literals)
        elif self.packet_type == 4:
            self._op_value = self.literal
        else:
            self._op_value = int([operator.gt, operator.lt, operator.eq][self.packet_type - 5](*self.literals))

    def has_more_sub_packets(self, count, byte_size):
        if self.length_type == 0:
            return byte_size < self.length
        else:
            return count < self.length

    def read_sub_packets(self):
        if self._sub_packets is None:
            sub_packet_count = 0
            sub_packet_bits_read = 0
            sub_packets = []
            while self.has_more_sub_packets(sub_packet_count, sub_packet_bits_read):
                sub_packet = PacketDecoder(self.unread)
                sub_packet.parse_packet()
                sub_packets.append(sub_packet)
                sub_packet_bits_read += sub_packet.bits_read
                sub_packet_count += 1
                self.read(sub_packet.bits_read)
            self._sub_packets = sub_packets

    @property
    def sub_packets(self):
        return self._sub_packets or []

    def parse_packet(self):
        try:
            self.read_version()
            self.read_packet_type()
            self.version_count += self.version
            if self.packet_type == 4:
                self.read_literal()
                self.literals.append(self.literal)
            else:
                self.read_length_type()
                self.read_length()
                self.read_sub_packets()
                for sub_packet in self.sub_packets:
                    self.version_count += sub_packet.version_count
                    self.literals.append(sub_packet.op_value)
            self.do_operation()
        except Exception as e:
            print(f"Oh no exception: {e}")
            print(f"ver/type {self.version}/{self.packet_type}")
            print(f"subs: {len(self.sub_packets or [])}")
            print(f"read/unread: {self.read_data}/{self.unread}")
            raise

    def display(self):
        output = f"V{self.version}/Type {self.packet_type}"
        if self._packet_type == 4:
            output += f":Literal:{self._literal}"
        else:
            output += "\n->"
            for sub in self.sub_packets:
                output += f"\n{sub.display()}"
            output += "\n<-"
        return output


def decode_data(data):
    binary = bin(int(data, 16))
    binary = str(binary)[2:]
    if len(binary) < len(data) * 4:
        binary = "0" * ((len(data) * 4) - len(binary)) + binary
    return binary


def do(day, part):
    data = next(common.read_input(16, 1), None)
    binary_data = decode_data(data)
    print(binary_data)
    p = PacketDecoder(binary_data)
    p.parse_packet()
    print(p.display())
    print(f"Trailing bits: {p.unread}")
    print(f"Version Count: {p.version_count}")
    if part == 1:
        return p.version_count
    if part == 2:

        print(f"Value: {p.op_value}")
        return p.op_value


from collections import Counter
import common
import math


def calculate_alignment_cost(crabs, dest):
    cost = 0
    for crab in crabs:
        cost += abs(crab - dest)
    return cost


def calculate_exponential_alignment_cost(crabs, dest):
    total_cost = 0
    for crab in crabs:
        cost = sum([i+1 for i in range(abs(crab-dest))])
        total_cost += cost
    return total_cost


def find_minimum_destination(crabs):
    return crabs[len(crabs)//2]


def find_exponential_minimum_destination(crabs):
    mean = sum(crabs) // len(crabs)
    # TODO: rounding up or down depends on something that I don't know
    return int(round(mean))


def do(day, part):
    crabs = [int(x) for x in next(common.read_input(day, part, "example")).split(',')]

    if part == 1:
        crabs = sorted(crabs)
    dest = find_minimum_destination(crabs) if part == 1 else find_exponential_minimum_destination(crabs)
    total_cost = calculate_alignment_cost(crabs, dest) if part == 1 else calculate_exponential_alignment_cost(crabs, dest)
    print(f"crabs: {crabs}\ndest: {dest}, cost: {total_cost}")

    return total_cost

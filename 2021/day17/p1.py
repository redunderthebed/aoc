import common
import re


def test_trajectory(velocity_x, velocity_y, target):
    pos_x, pos_y = (0, 0)
    max_height = 0
    while pos_y > target['y_max']:
        pos_x += velocity_x
        pos_y += velocity_y
        if pos_y > max_height:
            max_height = pos_y
        velocity_x = max(0, velocity_x - 1)
        velocity_y = velocity_y - 1
    return max_height


def do(day, part):
    line = next(common.read_input(day, part))
    x_min, x_max, y_min, y_max = map(int, re.match(
        r"target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)", line).groups())
    target = {
        'x_min': x_min, 'x_max': x_max, 'y_max': y_max, 'y_min': y_min
    }
    # test_trajectory(6, 9, target)
    return test_trajectory(0, abs(target['y_min']) - 1, target)

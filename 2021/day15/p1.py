import common
import networkx as nx


def transpose_space(space, transpose, start, dims):
    for pos, value in space.iterate_map(start, dims):
        if value is None:
            print(pos)
        transposed = space._move(pos, transpose, 1)
        value = value + 1
        space.set(value if value < 10 else 1, *transposed)


def do(day, part):
    space = common.Space()
    for line in common.read_input(day, part):
        space.append(line, clean=int)

    if part == 2:

        dims = tuple(x for x in space.dimensions)

        for x in range(1, 5):
            transpose = (dims[0], 0)
            start = (dims[0] * (x - 1), 0)
            transpose_space(space, transpose, start, dims)

        dims = tuple(x for x in space.dimensions)

        for y in range(1, 5):
            transpose = (0, dims[1])
            start = (0, dims[1] * (y - 1))
            transpose_space(space, transpose, start, dims)

    graph = nx.DiGraph()
    for pos, value in space.iterate_map():
        for n_pos, n_value in space.iterate_diamond(pos):
            if n_value is not None:
                graph.add_edge(pos, n_pos, weight=int(n_value))

    path = nx.astar_path(graph, (0, 0), tuple([x-1 for x in space.dimensions]))
    return sum(space.get(*pos) for pos in path[1:])




import common

unique_lengths = {
    2: 1,
    4: 4,
    3: 7,
    7: 8
}

correct_numbers = {
    'abcefg': 0,
    'cf': 1,
    'acdeg': 2,
    'acdfg': 3,
    'bcdf': 4,
    'abdfg': 5,
    'abdefg': 6,
    'acf': 7,
    'abcdefg': 8,
    'abcdfg': 9
}


def determine_numbers_by_length(segments):
    numbers = {}
    for segment in segments:
        number = unique_lengths.get(len(segment), None)
        if number:
            numbers[segment] = number
    return numbers


def count_known_numbers(known_numbers, display):
    count = 0
    for number in display:
        if known_numbers.get(number, None) is not None:
            count += 1
    return count


def deduce_segment_map(known_numbers, segments):
    known_segments = {v: k for k, v in known_numbers.items()}
    segment_map = {}

    # Find A from 1 and 7
    one = set(known_segments[1])
    seven = set(known_segments[7])
    segment_map['a'] = list(seven - one)[0]

    # Find c and f from 1
    segment_map['cf'] = set(known_segments[1])
    # Find b and d from 4 and c and f
    segment_map['bd'] = set(known_segments[4]) - segment_map['cf']

    # Determine which six segment number is zero (Doesn't have both b and d)
    six_segments = [x for x in segments if len(x) == 6]
    zero = [x for x in six_segments if segment_map['bd'].intersection(set(x)) != segment_map['bd']]
    known_segments[0] = zero[0]
    six_segments.remove(zero[0])

    # Find e as difference between 6 and 9 and 1
    difference = set(six_segments[0]).symmetric_difference(set(six_segments[1]))
    difference = list(difference - segment_map['cf'])[0]
    segment_map['e'] = difference

    # Find f as difference between 6 and 9 (and therefore find c)
    six = [x for x in six_segments if difference in x]
    known_segments[6] = six[0]
    six_segments.remove(six[0])
    known_segments[9] = six_segments[0]
    segment_map['f'] = list(set(known_segments[6]).intersection(segment_map['cf']))[0]
    segment_map['c'] = list(segment_map['cf'] - set(segment_map['f']))[0]

    zero = zero[0]
    segment_map['d'] = list(segment_map['bd'] - set(zero))[0]
    segment_map['b'] = list(segment_map['bd'] - set(segment_map['d']))[0]
    for key in list(segment_map.keys()):
        if len(key) > 1:
            segment_map.pop(key)

    segment_map['g'] = list(set('abcdefg') - set(segment_map.values()))[0]

    return segment_map


def find_numbers(display, segment_map):
    for number in display:
        rewired = ''.join([segment_map[x] for x in number])
        print(f"{number} rewired is {rewired}")
        rewired = ''.join(sorted(rewired))
        yield correct_numbers[rewired]


def do(day, part):
    total = 0
    for line in common.read_input(day, part):
        segments, display = line.split(' | ')
        segments = [''.join(sorted(x)) for x in segments.split(' ')]
        display = [''.join(sorted(x)) for x in display.split(' ')]

        print(segments, display)
        known_numbers = determine_numbers_by_length(segments)
        segment_map = deduce_segment_map(known_numbers, segments)
        correction_map = {v:k for k,v in segment_map.items()}
        display_numbers = int(''.join([str(number) for number in find_numbers(display, correction_map)]))
        print(display_numbers)
        total += display_numbers
    return total

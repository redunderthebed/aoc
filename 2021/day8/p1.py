import common

unique_lengths = {
    2: 1,
    4: 4,
    3: 7,
    7: 8
}


def determine_numbers_by_length(segments):
    numbers = {}
    for segment in segments:
        number = unique_lengths.get(len(segment), None)
        if number:
            numbers[segment] = number
    return numbers


def count_known_numbers(known_numbers, display):
    count = 0
    for number in display:
        if known_numbers.get(number, None) is not None:
            count += 1
    return count


def do(day, part):
    total = 0
    for line in common.read_input(day, part):
        segments, display = line.split(' | ')
        segments = [''.join(sorted(x)) for x in segments.split(' ')]
        display = [''.join(sorted(x)) for x in display.split(' ')]

        print(segments, display)
        known_numbers = determine_numbers_by_length(segments)
        count = count_known_numbers(known_numbers, display)
        print(f"known numbers: {known_numbers}, count: {count}")
        total += count
    return total
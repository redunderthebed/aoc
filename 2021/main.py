import argparse
import importlib
import json
import requests
import os

class AnswerCache(object):
    def __init__(self, filepath, day, path):
        self.filepath = filepath
        self._key = "{}-{}".format(day, path)
        self._data = None

    @property
    def data(self):
        if self._data is None:
            self._data = self._load()
        return self._data

    def add_guess(self, guess):
        if self._key not in self.data:
            self.data[self._key] = list()
        if guess not in self.data[self._key]:
            self.data[self._key].append(guess)

    def has_guess(self, guess):
        return guess in self.data.get(self._key, set())

    def _load(self):
        try:
            with open(self.filepath, "r") as file:
                return json.loads(file.read() or "{}")
        except FileNotFoundError:
            return dict()

    def save(self):
        with open(self.filepath, "w") as file:
            return file.write(json.dumps(self._data))


def submit_answer(answer, day, part, submit):
    cache = AnswerCache("guesses", day, part)
    is_answer_old = cache.has_guess(answer)
    if is_answer_old:
        print(f"[ Old answer ]")

    print(f"The answer for day {day} part {part} is: {answer}")

    if submit and not is_answer_old:
        url = f"https://adventofcode.com/2021/day/{day}/answer"
        response = requests.post(
            url,
            data={
                "level": part,
                "answer": result,
            },
            cookies={"session": os.getenv("AOC_SESSION_TOKEN")})

        if response.status_code == 200:
            cache.add_guess(result)
            too_soon = False
            if "That's not the right answer" in response.text:
                print("Incorrect answer!")
            elif "You gave an answer too recently" in response.text:
                start = response.text.index("You gave an answer too recently")
                end = response.text.index("</a>", start)
                print(response.text[start:end])
                too_soon = True
            else:
                print("Answer accepted...I think?")

            if not too_soon:
                cache.save()

            with open('last_response.html', 'w') as file:
                file.write(response.text)
        else:
            print(response.status_code, os.getenv("AOC_SESSION_TOKEN"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Advent of Code 2020")
    parser.add_argument("day", type=int, help="number of the day")
    parser.add_argument("--part", type=int, default=1, help="part number")
    parser.add_argument("--run", type=str, default="do", help="other func to run")
    parser.add_argument("--submit", action='store_true', default=False, help="Try submit result")
    args = parser.parse_args()
    try:
        mod = importlib.import_module(f"day{args.day}.p{args.part}")
    except ModuleNotFoundError as mnf:
        print(f"couldn't find part {args.part} doing part 1 instead")
        mod = importlib.import_module(f"day{args.day}.p1")

    if hasattr(mod, args.run):
        result = getattr(mod, args.run)(args.day, args.part)
        if result is not None:
            submit_answer(result, args.day, args.part, submit=args.submit)

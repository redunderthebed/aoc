import common


def do(day, part):
    pos = (0, 0)
    aim = 0
    for line in common.read_input(day, part):
        direction, distance = line.split(' ')
        distance = int(distance)
        if direction == "down":
            aim += distance
        if direction == "up":
            aim -= distance
        if direction == "forward":
            vector = (1, aim)
            pos = common.move(pos, vector, distance)
    print(f"Forward: {pos[0]} Depth: {pos[1]}")
    return pos[0] * pos[1]

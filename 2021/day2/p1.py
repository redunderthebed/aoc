import common

directions = {
    "forward": (1, 0),
    "up": (0, -1),
    "down": (0, 1)
}


def dir_to_vec(direction):
    return directions[direction]


def do(day, part):
    pos = (0, 0)
    for line in common.read_input(day, part):
        direction, distance = line.split(' ')
        distance = int(distance)
        vector = dir_to_vec(direction)
        pos = common.move(pos, vector, distance)
    print(f"Forward: {pos[0]} Depth: {pos[1]}")
    return pos[0] * pos[1]

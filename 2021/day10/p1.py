import common

opening_symbols = "([{<"
closing_symbols = ")]}>"
closer_map = {o: c for o, c in zip(closing_symbols, opening_symbols)}


class CorruptedChunk(Exception):
    symbol_scores = {')': 3, ']': 57, '}': 1197, '>': 25137}

    def __init__(self, illegal_char):
        self.illegal_char = illegal_char
        super(Exception, self).__init__(f"Found illegal_char: '{illegal_char}'")

    def score(self):
        return CorruptedChunk.symbol_scores[self.illegal_char]


class IncompleteLine(Exception):
    symbol_scores = {symbol: score for symbol, score in zip("([{<", range(1,5))}

    def __init__(self, missing_sequence):
        self.missing_sequence = missing_sequence
        super(Exception, self).__init__(f"Line is missing closing symbols: {missing_sequence}")

    def score(self):
        total_score = 0
        for x in reversed(self.missing_sequence):
            total_score *= 5
            total_score += IncompleteLine.symbol_scores[x]
        return total_score


def check_line(line):
    open_chunks = []
    for c in line:
        if c in opening_symbols:
            open_chunks.append(c)
        elif c in closer_map.keys():
            closed_chunk = open_chunks.pop(-1)
            if closed_chunk != closer_map[c]:
                raise CorruptedChunk(c)
    if len(open_chunks) > 0:
        raise IncompleteLine(open_chunks)


def do(day, part):

    corrupt_score = 0
    incomplete_scores = []
    for line in common.read_input(day, part):
        try:
            check_line(line)
        except CorruptedChunk as cc:
            corrupt_score += cc.score()
        except IncompleteLine as il:
            if part == 2:
                incomplete_scores.append(il.score())
                print(il.missing_sequence)

    if part == 1:
        return corrupt_score
    else:
        print(incomplete_scores)
        incomplete_scores.sort()
        print(incomplete_scores)
        return incomplete_scores[len(incomplete_scores)//2]









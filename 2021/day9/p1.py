import common


def is_lower_than_neighbours(height_map, center, center_value):
    for pos, value in height_map.iterate_neighbours(center):
        if value is not None and value <= center_value:
            return False
    return True


def do(day, part):
    height_map = common.Space(n_dims=2)
    for line in common.read_input(day, part):
        height_map.append(line, clean=int)
    low_points = []
    for pos, value in height_map.iterate_map():
        if is_lower_than_neighbours(height_map, pos, value):
            low_points.append((pos, value))

    return sum([value + 1 for pos, value in low_points])
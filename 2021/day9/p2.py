import common
import math

def is_lower_than_neighbours(height_map, center, center_value):
    for pos, value in height_map.iterate_neighbours(center):
        if value is not None and value <= center_value:
            return False
    return True


def find_basin(height_map, low_point):
    basin_points = set((low_point,))
    to_explore = set((low_point,))
    explored = set()
    while len(to_explore):
        basin_point = next((x for x in to_explore))
        for pos, value in height_map.iterate_diamond(basin_point):
            if pos not in explored and value is not None and value > height_map.get(basin_point) and value != 9:
                basin_points.add(pos)
                to_explore.add(pos)
        to_explore.remove(basin_point)
        explored.add(basin_point)
    return basin_points



def do(day, part):
    height_map = common.Space(n_dims=2)
    for line in common.read_input(day, part):
        height_map.append(line, clean=int)
    low_points = []
    for pos, value in height_map.iterate_map():
        if is_lower_than_neighbours(height_map, pos, value):
            low_points.append((pos, value))

    print(f"Low points: {low_points}")
    basins = []
    for pos, value in low_points:
        basins.append(find_basin(height_map, pos))
    basin_sizes = [len(x) for x in basins]
    print(f"Basin sizes: {basin_sizes}")
    basin_sizes.sort()
    return math.prod(basin_sizes[-3:])
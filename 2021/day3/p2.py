import common


def split_on_bit(numbers, index):
    zeros, ones = [], []
    for number in numbers:
        if number[index] == "0":
            zeros.append(number)
        else:
            ones.append(number)
    return zeros, ones


def select_most_common(zeros, ones):
    if len(ones) >= len(zeros):
        return ones
    else:
        return zeros


def select_least_common(zeros, ones):
    if len(ones) < len(zeros):
        return ones
    else:
        return zeros


def find_code(numbers, selector):
    bit = 0
    while len(numbers):
        zeros, ones = split_on_bit(numbers, bit)
        numbers = selector(zeros, ones)
        if len(numbers) > 1:
            bit += 1
        else:
            return numbers[0]


def do(day, part):
    numbers = [line for line in common.read_input(day, part)]
    oxy_code = find_code(numbers, select_most_common)
    carbon_code = find_code(numbers, select_least_common)
    print(oxy_code, int(oxy_code, 2), carbon_code, int(carbon_code, 2))
    return int(oxy_code, 2) * int(carbon_code, 2)


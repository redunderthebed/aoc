import common

def do(day, part):
    input_file = None
    total = [0] * len(next(common.read_input(day, part, input_file)))
    for line in common.read_input(day, part, input_file):
        for i in range(len(line)):
            if line[i] == "0":
                total[i] -= 1
            else:
                total[i] += 1
    print(total)
    gamma = ''.join(["1" if x > 0 else "0" for x in total])
    epsilon = ''.join(["0" if x > 1 else "1" for x in total])
    print(epsilon, int(epsilon, 2), gamma, int(gamma, 2))
    return int(epsilon, 2) * int(gamma, 2)


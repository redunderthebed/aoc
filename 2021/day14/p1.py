import common
import re
from collections import deque, Counter


def do(day, part):
    lines = common.read_input(day, part)
    polymer = deque(next(lines))
    reactions = {}
    for line in lines:
        reagents, reactant = re.match(r"(\w\w) -> (\w)", line).groups()
        reactions[reagents] = reactant

    print(''.join(polymer))
    for i in range(10 if part == 1 else 40):
        polymer_len = range(len(polymer) - 1)
        print(f"round {i}: len = {polymer_len}")
        for _ in polymer_len:
            reagents = polymer[0] + polymer[1]
            polymer.rotate(-1)
            if reagents in reactions:
                polymer.appendleft(reactions[reagents])
                polymer.rotate(-1)
        polymer.rotate(-1)

    counter = Counter(polymer)

    return counter.most_common()[0][1] - counter.most_common()[-1][1]


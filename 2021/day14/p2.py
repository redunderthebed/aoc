import common
import re
from collections import deque, Counter
import networkx as nx


def process_pair(reactions, a,b):
    if a+b in reactions:
        return reactions[a+b]
    else:
        return ''


def remove_static(old, new):
    left = []
    clean = ""
    counter = Counter()
    for i in range(len(old)):
        if old[i] == new[i]:
            left.append(new[i])
        else:
            if left:
                if len(left) <= 6:
                    clean += ''.join(left)
                else:
                    clean += left[0] + "_" + left[-1]
                    counter.update(left[1:-1])
                left = []
            clean += new[i]
    clean = clean + new[len(old):]
    return clean, counter


def do(day, part):
    lines = common.read_input(day, part)
    polymer = next(lines)
    reactions = {}
    for line in lines:
        reagents, reactant = re.match(r"(\w\w) -> (\w)", line).groups()
        reactions[reagents] = reactant

    old_poly = None
    counter = Counter()
    for i in range(20):
        new_polymer = ""
        for a, b in common.pairwise(list(polymer)):
            new_polymer += a + process_pair(reactions, a, b)
        polymer = new_polymer + polymer[-1]
        if old_poly is not None:
            #polymer, removed = remove_static(old_poly, polymer)
            #counter.update(removed)
            print(f"{i}: {polymer[:100]} {Counter(polymer)}")
        old_poly = polymer




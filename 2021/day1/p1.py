import common
from common import pairwise


def do(day, part):
    return sum(1 for before, after in pairwise(common.read_input(day, part)) if int(before) < int(after))

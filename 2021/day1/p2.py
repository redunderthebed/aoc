import common
from common import pairwise, sliding


def do(day, part):
    readings = [int(line) for line in common.read_input(day, part)]

    windows = [sum(window) for window in sliding(readings, 3)]

    return sum(1 for before, after in pairwise(windows) if before < after)

import math
import common
import json
from binarytree import Node, get_parent

class SnailNumber(object):
    def __init__(self, tree):
        self.tree = tree

    @classmethod
    def get_node(cls, node_value, depth):
        if isinstance(node_value, list):
            node = Node(depth)
            node.left = cls.get_node(node_value[0], depth-1)
            node.right = cls.get_node(node_value[1], depth-1)
            return node
        else:
            return Node(node_value)

    @classmethod
    def from_line(cls, line):
        line = json.loads(line)
        return cls(cls.get_node(line, -1))

    @staticmethod
    def lower_tree(tree):
        for node in tree.inorder:
            if node.value < 0:
                node.value -= 1

    def find_leaf(self, node, dir):
        while(node.value < 0):
            if getattr(node, dir):
                node = getattr(node, dir)
            else:
                other = getattr(node, 'right' if dir == 'left' else 'left')
                if other:
                    node = other
                else:
                    raise ("OH NO")
        return node

    def combine_trees(self, other):
        self.lower_tree(self.tree)
        self.lower_tree(other.tree)
        new_root = Node(-1)
        new_root.left = self.tree
        new_root.right = other.tree
        self.tree = new_root

    def explode(self, node):
        leafs = [x for x in self.tree.inorder if x.value >= 0]
        if node.left:
            left_index = leafs.index(node.left)
            if left_index - 1 in range(len(leafs)):
                leafs[left_index - 1].value += node.left.value
        if node.right:
            right_index = leafs.index(node.right)
            if right_index + 1 in range(len(leafs)):
                leafs[right_index + 1].value += node.right.value
        parent = get_parent(self.tree, node)
        if parent.left == node:
            parent.left = Node(0)
        else:
            parent.right = Node(0)

    def split(self, node):
        parent = get_parent(self.tree, node)
        split_parent = Node(parent.value - 1)
        split_parent.left = Node(math.floor(node.value / 2))
        split_parent.right = Node(math.ceil(node.value / 2))
        if parent.left == node:
            parent.left = split_parent
        else:
            parent.right = split_parent

    @staticmethod
    def _magnitude(node):
        if node.value >= 0:
            return node.value
        else:
            return SnailNumber._magnitude(node.left) * 3 + SnailNumber._magnitude(node.right) * 2

    def magnitude(self):
        return SnailNumber._magnitude(self.tree)

    def add(self, other):
        self.combine_trees(other)

        clean = False
        while not clean:
            clean = True
            for node in list(self.tree.inorder):
                if node.value < -4:
                    self.explode(node)
                    clean = False
                    break
            if clean:
                for node in list(self.tree.inorder):
                    if node.value > 9:
                        self.split(node)
                        clean = False
                        break


def do(day, part):
    total = None
    for line in common.read_input(day, part):
        sn = SnailNumber.from_line(line)
        if total is None:
            total = sn
        else:
            total.add(sn)
    return total.magnitude()


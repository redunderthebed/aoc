import common


def find_winning_board(boards, numbers):
    for number in numbers:
        for board in boards:
            pos = board.lookup(number)
            if pos:
                board.remove(*pos)
                if all([val is None for pos, val in board.iterate_direction(pos, (1, 0), reverse_axis=0)]) \
                        or all([val is None for pos, val in board.iterate_direction(pos, (0, 1), reverse_axis=1)]):
                    return board, number


def find_losing_board(boards, numbers):
    losers = []
    for number in numbers:
        new_losers = []
        for board in boards:
            pos = board.lookup(number)
            if pos:
                board.remove(*pos)
                if all([val is None for pos, val in board.iterate_direction(pos, (1, 0), reverse_axis=0)]) \
                        or all([val is None for pos, val in board.iterate_direction(pos, (0, 1), reverse_axis=1)]):
                    new_losers.append(board)
        for loser in new_losers:
            boards.remove(loser)
            losers.append(loser)

        if len(boards) == 0:
            return losers[-1], number


def do(day, part):
    groups = common.read_input(day, part, input_name=None, group_lines=True)
    numbers = [int(x) for x in next(groups)[0].split(',')]
    boards = []
    for group in groups:
        board = common.LookupSpace(n_dims=2)
        x, y = 0, 0
        for line in group:
            for number in line.split(' '):
                if number:
                    board.set(int(number), x, y)
                    x += 1
            x = 0
            y += 1
        boards.append(board)
    if part == 1:
        final_board, final_number = find_winning_board(boards, numbers)
    else:
        final_board, final_number = find_losing_board(boards, numbers)
    first_row = sum([value for value in final_board.space.values()])
    print(first_row, final_number)
    return first_row * final_number


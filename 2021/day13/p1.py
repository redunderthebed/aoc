import common
import re

class Paper(common.Space):
    def __init__(self):
        super(Paper, self).__init__(n_dims=2)

    def remove(self, *pos):
        self.space.pop(pos)

    def fold(self, axis, fold_value):
        if axis == "y":
            for pos, value in self.iterate_map((0, fold_value), (self.dimensions[0], self.dimensions[1] - fold_value)):
                if value is not None:
                    self.remove(*pos)
                    mirrored = (pos[0], fold_value - (pos[1] - fold_value))
                    self.set("#", *mirrored)
            self.dimensions[1] = fold_value

        if axis == "x":
            for pos, value in self.iterate_map((fold_value, 0), (self.dimensions[0] - fold_value, self.dimensions[1])):
                if value is not None:
                    self.remove(*pos)
                    mirrored = (fold_value - (pos[0] - fold_value), pos[1])
                    self.set("#", *mirrored)
            self.dimensions[0] = fold_value


def do(day, part):
    paper = Paper()
    folds = []
    for line in common.read_input(day, part):
        if ',' in line:
            x, y = line.split(',')
            paper.set('#', int(x), int(y))
        else:
            result = re.match(r"fold along (x|y)=(\d+)", line)
            if result:
                axis, value = result.groups()
                folds.append((axis, int(value)))

    print(paper.view_map(empty_value="."))
    for axis, value in (folds[:1] if part == 1 else folds):
        paper.fold(axis, value)
        print(axis, value)
        print(paper.view_map(empty_value="."))
    if part == 1:
        return sum(1 for pos, value in paper.iterate_map() if value is not None)
    else:
        print("See output")

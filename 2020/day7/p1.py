import common
import re

def list_containers(inner, contained_by):
  count = set()
  for container in contained_by.get(inner, []):
    count.add(container[1])
    count.update(list_containers(container[1], contained_by))
  return count

def do(day, part):
  contained_by = {}
  for line in common.read_input(day, part): #, input_name="example.txt"):
    match = re.match("^(\w+\s\w+) bags contain (?:no other bags.)?", line)
    if match:
      container = match.groups()[0]
      line = line[match.end():]

      inners = re.findall("(\d \w+\s\w+ bags?[,\.])+", line)
      if inners:
        for inner in inners:
          match = re.match("(\d) (\w+\s\w+) bags?[,\.]", inner)
          if match:
            num_bags, bag_colour = match.groups()
            contained_by[bag_colour] = contained_by.get(bag_colour, [])
            contained_by[bag_colour].append((num_bags, container))

  return len(list_containers("shiny gold", contained_by))
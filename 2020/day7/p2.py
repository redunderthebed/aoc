import common
import re

def list_containers(container, contained_by):
  count = 0
  for inner in contained_by.get(container, []):
    print(inner)
    count += int(inner[0])
    count += int(inner[0]) * list_containers(inner[1], contained_by)
    print(count)
  return count

def do(day, part):
  contained_by = {}
  for line in common.read_input(day, part):
    match = re.match("^(\w+\s\w+) bags contain (?:no other bags.)?", line)
    if match:
      container = match.groups()[0]
      line = line[match.end():]
      #print('container', container)
      inners = re.findall("(\d \w+\s\w+ bags?[,\.])+", line)
      if inners:
        #print(inners)
        for inner in inners:
          match = re.match("(\d) (\w+\s\w+) bags?[,\.]", inner)
          if match:
            num_bags, bag_colour = match.groups()
            contained_by[container] = contained_by.get(container, [])
            contained_by[container].append((num_bags, bag_colour))
  return list_containers("shiny gold", contained_by)
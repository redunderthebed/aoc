import common
import functools
def find_seat(partition, low=0, high=127, top="B", bottom="F"):
  for x in partition:
    if x == bottom:
      high -= (high - low + 1) // 2
    if x == top:
      low += (high - low + 1) // 2
  if low != high:
    raise Exception(f"No convergance: {low} {high}")
  return low

def seat_id(row, col):
  return row * 8 + col

def process_seats(seats):
  for seat in seats:
    row = find_seat(seat[0:7], low=0, high=127, top="B", bottom="F")
    col = find_seat(seat[7:], low=0, high=7, top="R", bottom="L")
    yield seat_id(row, col)

def do(day, part):
  return max(process_seats(common.read_input(day, part)))

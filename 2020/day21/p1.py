import common
import re
from collections import Counter
def do(day, part):
  input_name = "example.txt"
  input_name = None
  contains = {}
  total_ings = set()
  ing_count = Counter()
  #input_name = None
  for line in common.read_input(day, part, input_name=input_name):
    match = re.match("((?:(?:\w+)\s)+)(?:\(contains ((?:\w+(?:,?\s?))+))", line)
    if match:
      #print(match.groups()[0].strip().split(' '), )
      ings = match.groups()[0].strip().split(' ')
      allergens = match.groups()[1].replace(' ', '').split(',')
      for allergen in allergens:
        contains[allergen] = contains.get(allergen, [])
        contains[allergen].append(set([x for x in ings]))
        total_ings.update(set(ings))
      ing_count.update(ings)
  print(contains)
  found = {}
  mysteries = {}
  
  for allergen, recipes in contains.items():
    total = None
    for recipe in recipes:
      if total is None:
        total = recipe
      else:
        total.intersection_update(recipe)
    if len(total) == 1:
      found[list(total)[0]] = allergen
    else:
      mysteries[allergen] = total
  print(found)
  print(mysteries)

  safety = 0
  while len(mysteries) > 0 and safety < 100:
    solved = None
    for allergen, options in mysteries.items():
      new_solved = []

      for option in options:
        if option in found:
          print('solved', option, found[option])
          new_solved.append((found[option], option))

      for _, ing in new_solved:
        options.difference_update(set([ing]))

      if len(mysteries[allergen]) == 1:
        solved = (allergen, list(mysteries[allergen])[0])
        print("solved", solved[0], '=', solved[1])
        break

    if solved:
      found[solved[1]] = solved[0]
      mysteries.pop(solved[0])

    safety += 1
  print(found)
  print(mysteries)
  cool_ings = total_ings - found.keys()
  print(cool_ings)
  print(ing_count)
  if part == 1:
    return sum([ing_count[ing] for ing in cool_ings])
  else:
    return ",".join([x[0] for x in sorted(list(found.items()), key=lambda x: x[1])])



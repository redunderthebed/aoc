import common
tried = [2633]
def do(day, part):
  area = common.Map(0,0)
  input_name = "example.txt"
  input_name = None
  for row in common.read_input(day, part, input_name):
    area.append(row)
  if part == 1:
    max_neighbours = 4
    n_func = 'iter_surrounds'
  else:
    max_neighbours = 5
    n_func = 'iter_view'
  area.view(0, 0, area.width, area.height)
  while True:
    new_map = area.copy()
    for y in range(area.height):
      for x in range(area.width):
        v = area.get(x,y)
        if v in ["L", "#"]:
          neighbours = sum([1 for x in getattr(area, n_func)(x, y) if x == '#'])
          if v == "L" and neighbours == 0:
            new_map.set(x, y, "#")
          if v == "#" and neighbours >= 5:
            new_map.set(x, y, "L")
    if new_map.map == area.map:
      break
    else:
      area = new_map

  count = sum([1 for x in new_map.iterate_map() if x == '#'])
  print(count)
  area.view(0, 0, area.width, area.height)
  return count


import common

def do(day, part):
  input_name = "example.txt"
  input_name = None
  x,y = 0,0
  space = common.Space(n_dims=3 if part else 4)

  lines = []
  for line in common.read_input(day, part, input_name):
    lines.append([x for x in line])
  if part == 1:
    space.append([lines])
  else:
    space.append([[lines]])
  print(space.view_map())
  
  for turn in range(6):
    old_space = space.copy()
    for point, value in old_space.iterate_map(pos=-1, extra=1, empty_value="."):
      num_active_neighbours = sum(
        [1 for n_point, n_val in old_space.iterate_neighbours(point, empty_value=".") if n_val == "#"]
        )
      
      if value == "#" and num_active_neighbours not in [2,3]:
        space.set(".", point)
      elif value == "." and num_active_neighbours == 3:
        space.set('#', point)
    print(space.view_map(empty_value="."))
  return sum(1 for p,v in space.space.items() if v=="#")

import common
import math

def do(day, part_number):
  map = common.Map(0, 0)

  if part_number == 1:
    slopes = [(3,1)]
  else:
    slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]

  for line in common.read_input(day, part_number):#, input_name="example.txt"):
    map.append(line)
  
  #map.view(0, map.height - 20, len(line) *3, 22, value=".")

  tree_slopes = []
  for slope in slopes:
    trees = 0
    pos = (0, 0)
    while(pos[1] < map.height):
      pos = (pos[0] + slope[0], pos[1] + slope[1])
      if map.get_wrapped(pos[0], pos[1]) == "#":
        trees += 1
    tree_slopes.append(trees)

  print(tree_slopes)
  trees = math.prod(tree_slopes)

  return trees
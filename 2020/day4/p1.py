import common
import re

def process_passport(passports, passport):
  req_fields = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}
  opt_fields = {'cid'}
  valid = False
  passports.append(passport)
  missing_fields = req_fields - set(passport.keys())
  if missing_fields == set():
    print("valid passport", passport.keys())
    valid = True
  else:
    print("missing fields", missing_fields)
  return valid

def do(day, part_number):
  

  passports = []
  passport = {}
  valid_count = 0
  for line in common.read_input(day, part_number):
    if line.strip() == '':
      valid_count += process_passport(passports, passport)
      passport = {}
      continue
    for field, value in re.findall("(\w\w\w):([#\w\d]+)", line):
      passport[field] = value
  valid_count += process_passport(passports, passport)
  return valid_count
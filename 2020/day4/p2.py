import common
import re

req_fields = {
    'byr': "(19[2-9]\d|200[0-2])",
    'iyr': "(201\d|2020)",
    "eyr": "(202\d|2030)",
    "hgt": "(1[5-8]\dcm|19[0-3]cm|59in|6\din|7[0-6]in)",
    "hcl": "(#[\d\w]{6})$",
    "ecl": "(amb|blu|brn|gry|grn|hzl|oth)",
    "pid": "(\d{9})$",
  }

opt_fields = {
  'cid'
}

def test_fields(day, part):
  valid_fields = ['byr:2000', 'hgt:158cm', 'hgt:182cm', 'hgt:193cm', 'hgt:59in', 
    "pid:000000001", "pid:123456789"]
  invalid_fields = ['byr:1902', 'hgt:194cm', 'hgt:150', 'hgt:58in', 'iyr:2000', 
    "pid:1234567890"]

  for sample in valid_fields:
    field, value = re.match("(\w\w\w):([#\w\d]+)", sample).groups()
    match = re.match(req_fields[field], value)
    if not match:
      print(f"VF Failure: {sample}")

  for sample in invalid_fields:
    field, value = re.match("(\w\w\w):([#\w\d]+)", sample).groups()
    match = re.match(req_fields[field], value)
    if match:
      print(f"IVF Failure: {sample}")
  return None

def process_passport(passports, passport):
  valid = False
  passports.append(passport)
  missing_fields = set(req_fields.keys()) - set(passport.keys())
  if missing_fields == set():

    valid = True
    for field, value in passport.items():
      if field not in opt_fields:
        match = re.match(req_fields[field], value)
        if match is None:
          return False
    print("valid passport", passport.keys())
  else:
    print("missing fields", missing_fields)
  return valid

def do(day, part_number):
  passports = []
  passport = {}
  valid_count = 0
  for line in common.read_input(day, part_number):
    if line.strip() == '':
      valid_count += process_passport(passports, passport)
      passport = {}
      continue
    for field, value in re.findall("(\w\w\w):([#\w\d]+)", line):
      passport[field] = value
  valid_count += process_passport(passports, passport)
  return valid_count
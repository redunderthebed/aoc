import common
import itertools
tried = [7202279, 7409241]

def do(day, part):
  preamble_length = 5
  target = 57195069
  #target = 127
  input_name = None
  #input_name = "example.txt"
  numbers = (x for x in common.read_input(day, part, input_name=input_name))
  numbers = [int(x) for x in numbers if x]
  slide = []
  for x in numbers:
    slide.append(x)
    if sum(slide) == target:
      return slide
    while sum(slide) > target:
      slide = slide[1:]
      if sum(slide) == target:  
        print(sum(slide), target, min(slide), max(slide), slide)
        return min(slide) + max(slide)

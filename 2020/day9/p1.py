import common
import itertools
def do(day, part):
  preamble_length = 25
  print([int(x) for x in common.read_input(day, part) if x])
  numbers = [int(x) for x in common.read_input(day, part) if x]#, input_name="example.txt")]
  for i in range(preamble_length, len(numbers)):
    result = numbers[i]
    found = False
    for c in itertools.combinations(numbers[i-preamble_length: i], 2):
      if sum(c) == result:
        found = True

    if found is False:
      print(i)
      return result
import common
from collections import deque

class CrabCups(common.Game):
  def round_start(self, round_num):
    cups = self.players[1]['cups']
    max_cup = len(cups)
    current = cups[0]
    cups.rotate(-1)
    pickup = (cups.popleft(), cups.popleft(), cups.popleft())
    destination = current - 1
    while destination in pickup or destination <= 0:
      destination = destination - 1
      if destination <= 0:
        destination = max_cup
    while cups[0] != destination:
      cups.rotate(1)
    cups.rotate(-1)
    cups.extend(pickup)
    while cups[0] != current:
      cups.rotate(1)
    cups.rotate(-1)
    if round_num == 100:
      self.winner = self.players[1]
  

def do(day, part):
  input_name = "example.txt"
  input_name = None
  cups = deque()
  game = CrabCups()
  for line in common.read_input(day, part, input_name=input_name):
  	cups.extend([int(x) for x in line])
  game.add_player(1, {'cups': cups})
  game.play()
  cups = game.winner['cups']
  while cups[0] != 1:
    cups.rotate(1)
  return ''.join([str(x) for x in cups])[1:]

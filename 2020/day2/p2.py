import common
import itertools
import math
import re

def do(day, part_number):
  valid_count = 0
  for x in common.read_input(day, 1):
    match = re.match("(\d+)-(\d+) ([\d\w]): ([\d\w]+)", x)
    if match is None:
      print(f"Failed to find match for: {x}")
    first, second, letter, pw = match.groups()
    first, second = int(first) - 1, int(second) - 1
    good = 0
    if first < len(pw) and pw[first] == letter:
      good += 1
    if second < len(pw) and pw[second] == letter:
      good += 1

    if good == 1:
      valid_count += 1
    print(x, first, second, pw, 
      pw[first] if first < len(pw) else None, 
      pw[second] if second < len(pw) else None, good==1)
  return valid_count
      
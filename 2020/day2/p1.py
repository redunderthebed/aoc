import common
import itertools
import math
import re

def do(day, part_number):
  valid_count = 0
  for x in common.read_input(day, part_number):
    match = re.match("(\d+)-(\d+) ([\d\w]): ([\d\w]+)", x)
    if match is None:
      print(f"Failed to find match for: {x}")
    low, high, letter, pw = match.groups()
    low, high = int(low), int(high)
    char_count = len(re.findall(letter, pw))
    if low <= char_count <= high:
      valid_count += 1
  return valid_count
      
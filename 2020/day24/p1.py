import common
import re

def do(day, part):
  input_name = "example.txt"
  input_name = None
  space = common.Space(n_dims=2)
  for line in common.read_input(day, part, input_name=input_name):
    moves = re.findall("(e|se|sw|w|nw|ne)", line)
    point = (0,0)
    for move in moves:
      if move == "e":
        point = (point[0] + 1, point[1])
      elif move == 'se':
        point = (point[0] + (1 if point[1] % 2 == 0 else 0), point[1] + 1)
      elif move == 'sw':
        point = (point[0] - (1 if point[1] % 2 == 1 else 0), point[1] + 1)
      elif move == 'w':
        point = (point[0] - 1, point[1])
      elif move == 'nw':
        point = (point[0] - (1 if point[1] % 2 == 1 else 0), point[1] - 1)
      elif move == 'ne':
        point = (point[0] + (1 if point[1] % 2 == 0 else 0), point[1] - 1)
    current = space.get(point, value='w')
    if current == 'w':
      space.set('b', point)
    else:
      space.set('w', point)
  total_black = 0
  for point, value in space.space.items():
    if value == 'b':
      total_black += 1
  return total_black
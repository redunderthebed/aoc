import common
import re
import math

tried = [3546177076502, 546177076502]

def dec_to_bin(number, length=36):
  bits = ""
  for i in range(length - 1, -1, -1):
    v = 2 ** i
    if v <= number:
      bits += "1"
      number -= v
    else:
      bits += "0"
  return bits

def mask_bin(bin_num, mask):
  masked = []
  l = max(len(bin_num), len(mask))
  for a,b in zip(("0" * (l - len(bin_num))) + bin_num, 
    ("X" * (l-len(mask))) + mask):
    if b == "X":
      masked.append(a)
    else:
      masked.append(b)
  return "".join(masked)

def bin_to_dec(bits):
  v = 0
  l = len(bits)
  for i in range(l-1, -1, -1):
    if bits[(l-1) - i] == "1":
      v += 2 ** i
  return v

def do(day, part):
  input_name = "example.txt"
  input_name = None
  mask = "X" * 36
  mem_space = {}
  for line in common.read_input(day, part, input_name):
    match = re.match("mask = ([X10]+)", line)
    if match:
      mask = match.groups()[0]
      print("mask", mask)
    match = re.match("mem\[(\d+)\] = ([\d]+)", line)
    if match:
      addr, number = match.groups()
      number = int(number)
      bits = dec_to_bin(number)
      mem_space[addr] = mask_bin(bits, mask)
      print(mask)
      print(mem_space[addr], addr, number)
  total = 0
  for bits in mem_space.values():
    total += bin_to_dec(bits)
  #print(mem_space)
  return total

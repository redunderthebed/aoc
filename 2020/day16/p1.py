import common
import re

def do(day, part):
  input_name = "example.txt"
  input_name = None
  field_list, ticket, others = [], [], []
  buckets = [field_list, ticket, others]
  bucket_idx = 0
  for line in common.read_input(day, part, input_name):
    if line in ["your ticket:", "nearby tickets:"]:
      bucket_idx += 1
    else:
      buckets[bucket_idx].append(line)

  fields = {}
  for field in field_list:
    name, start1, end1, start2, end2 = re.match("([\w\s]+)\: (\d+)-(\d+) or (\d+)-(\d+)", field).groups()
    fields[name] = {
      "name": name, 
      "ranges": [range(int(start1), int(end1)+1), range(int(start2), int(end2)+1)]
    }

  bad_guys = []
  good_guys = []
  bad = 0
  good = 0
  for line in others:
    coverage = set()
    for value in line.split(','):
      value = int(value)
      found = False
      for k,v in fields.items():
        for r in v.get('ranges'):
          if value in r:
            found = True
            coverage.add(k)
      if found is False:
        bad += 1
        bad_guys.append(value)
        break
    if found is True:
      if len(coverage) < len(fields):
        bad += 1
      else:
        good += 1
        good_guys.append(line)

  return sum(bad_guys)

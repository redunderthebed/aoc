import argparse
import importlib
import os

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Advent of Code 2020")
  parser.add_argument("day", type=int, help="number of the day")
  parser.add_argument("--part", type=int, default=1, help="part number")
  parser.add_argument("--run", type=str, default="do", help="other func to run")

  args = parser.parse_args()
  try:
    mod = importlib.import_module(f"day{args.day}.p{args.part}")
  except ModuleNotFoundError as mnf:
    print(f"couldn't find part {args.part} doing part 1 instead")
    mod = importlib.import_module(f"day{args.day}.p1")

  if hasattr(mod, args.run):
    result = getattr(mod, args.run)(args.day, args.part)
    if result in getattr(mod, 'tried', []):
      print(f"Old answer")

  print(f"The answer for day {args.day} part {args.part} is: {result}")

import common
import re

class CrabGame(common.Game):
  def turn_start(self, id, player):
    print(f"Player {id}'s deck: {player['deck']}")
    draw = player['deck'][0]
    player['deck'] = player['deck'][1:]
    player['draw'] = draw
    print(f"Player {id} plays: {draw}")

  def round_start(self, game_round):
    print(f"-- Round {game_round} --")

  def round_end(self, game_round):
    draws = sorted([(self.players[pid]['draw'], self.players[pid], pid) for pid in self.player_ids], key=lambda x: x[0])
    loser, winner = draws
    winner[1]['deck'] = winner[1]['deck'] + [winner[0], loser[0]]
    if len(loser[1]['deck']) == 0:
      self.winner = winner[1]
    print(f"Player {winner[2]} wins the round!")

def do(day, part):
  input_name = "example.txt"
  input_name = None
  game = CrabGame()
  for group in common.read_input(day, part, input_name=input_name, group_lines=True):
    player_id = int(re.match("Player (\d):", group[0]).groups()[0])
    game.add_player(player_id, {'deck': [int(x) for x in group[1:]]})
  print(game.player_ids)
  game.play()
  print(game.players)
  return sum([(m*int(x)) for m,x in enumerate(game.winner['deck'][::-1], 1)])
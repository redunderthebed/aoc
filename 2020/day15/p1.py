import common

tried = [697, 342]
def do(day, part):
  #input_name = "example.txt"
  input_name = None
  last_spoken = [None] + [int(x) for x in list(common.read_input(day, part, input_name))[0].split(',')]
  #last_spoken = [None, 3, 1, 2]
  history = {}
  for i in range(1, len(last_spoken) - 1):
    history[last_spoken[i]] = i

  for i in range(len(last_spoken) - 1, 30000000):
    history[last_spoken[-2]] = i -1
    num = last_spoken[-1]
    last_index = history.get(num)
    if last_index is None:
      next_spoken = 0
    else:
      next_spoken = i - last_index
    last_spoken.append(next_spoken)
    
  return next_spoken


  

import common
import matplotlib.pyplot as plt

def transform(loop_number, subject_number=7, value=1, lim=20201227):
  for _ in range(loop_number):
    value = value * subject_number
    value = value % lim
  return value

def do(day, part):
  door_pk, card_pk = [int(x) for x in common.read_input(day, part)]
  # card_pk = 5764801
  # door_pk = 17807724
  subject_number = 7
  lim = 20201227
  value = 1
  print(f"Door PK: {door_pk} Card PK: {card_pk}")
  rainbow = {}
  loop_number = 0
  cap = 300
  card_loop = None
  door_loop = None
  while (card_loop is None and door_loop is None) and cap > 0:
    loop_number += 1
    value = value * subject_number
    value = value % lim
    if value in rainbow:
      print("duplicate!", value)
      import pdb; pdb.set_trace()
    else:
      rainbow[value] = loop_number
    if value == door_pk:
      print("Door loop number:", loop_number)
      door_loop = loop_number
    elif value == card_pk:
      print("Card loop number:", loop_number)
      card_loop = loop_number
    #print(value)
    #cap -= 1
    if loop_number % (20201227/10000) == 0:
      print((loop_number/20201227) * 100, "%")
  enc_key = None
  if door_loop is not None:
    enc_key = transform(door_loop, card_pk)
  else:
    enc_key = transform(card_loop, door_pk)

  return enc_key
  
  # plt.plot(rainbow.keys(), rainbow.values())
  # plt.show()

import common
import math

tried = [944, 1007275]
def do(day, part):
  #input_name = "test.txt"
  input_name = "example.txt"
  #input_name = None
  lines = common.read_input(day, part, input_name)
  start = int(next(lines))
  buses = []
  for bus in next(lines).split(','):
    if bus != "x":
      buses.append(int(bus))
    else:
      buses.append(1)
  print(buses)
  ts = 0
  import pdb; pdb.set_trace()
  for i in range(0, 3):
    prev = buses[0]
    ts += prev
    second = ((ts // buses[1]) + 1) * ts
    print(ts, second)

  # while True:
  #   prev = buses[0]
  #   ts += prev
  #   found = True
  #   for dist, bus in enumerate(list(reversed(buses))[1:], 1):
  #     second = ((ts // bus) + 1) * bus
  #     #print(f"ts:{ts} bus:{bus} dist:{dist} second:{second}")
  #     if second != ts + dist:
  #       found = False
  #       break
  #   if found:
  #     break

  # print(ts)
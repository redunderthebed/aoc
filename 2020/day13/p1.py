import common
import math

tried = [944, 1007275]
def do(day, part):
  input_name = "example.txt"
  input_name = None
  lines = common.read_input(day, part, input_name)
  start = int(next(lines))
  earliests = []
  for bus in next(lines).split(','):
    if bus != "x":
      bus = int(bus)
      earliest = ((start // bus) + 1) * bus
      print(bus, earliest)
      earliests.append((earliest-start, bus))
  return math.prod(min(earliests))
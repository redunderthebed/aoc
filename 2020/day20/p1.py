import common
import math
from common import Space

def do(day, part):
  input_name = "example.txt"
  input_name = None
  tiles = {}
  for group in common.read_input(day, part, input_name=input_name, group_lines=True):
    label = group[0].split(' ')[1].split(':')[0]
    space = Space(n_dims=2)
    print(label)
    for line in group[1:]:
      space.append([[x for x in line if x not in '\r\n']])
    top = space.view_map(pos=(0,0), size=(space.dimensions[0], 1)).replace('\n', '').replace('.', '0').replace('#', '1')
    bottom = space.view_map(pos=(0,space.dimensions[1] - 1), size=(space.dimensions[0], 1)).replace('\n', '').replace('.', '0').replace('#', '1')
    left = space.view_map(pos=(0,0), size=(1, space.dimensions[1])).replace('\n', '').replace('.', '0').replace('#', '1')
    right = space.view_map(pos=(space.dimensions[0]-1,0), size=(1, space.dimensions[1])).replace('\n', '').replace('.', '0').replace('#', '1')
    tiles[label] = {
      'sides': set([
        top, left, bottom, right]),
      'reversed': set([top[::-1], left[::-1], bottom[::-1], right[::-1]]),
      # {
      #   'top': top, 'left': left, 'bottom': bottom, 'right': right
      # },
      'space': space
      }
  corners = set()

  tile_labels = list(tiles.keys())
  for lbl_a in tile_labels:
    tile_a = tiles[lbl_a]
    matched = set()
    for lbl_b in tile_labels:
      if lbl_a == lbl_b:
        continue
      tile_b = tiles[lbl_b]
      matched.update(tile_a.get('sides').intersection(tile_b.get('sides')))
      matched.update(tile_a.get('reversed').intersection(tile_b.get('sides')))
    if len(matched) == 2:
      corners.add(int(lbl_a))
  return math.prod(corners)


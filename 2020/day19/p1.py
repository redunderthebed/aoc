import common
import re

def resolve_rule(rules, label):
  rule = rules[label]
  if rule.get('char'):
    return rule['char']

  if rule.get('meta') and not rule.get('options'):
    options = rule.get('meta').split('|')
    rule['options'] = []
    for option in options:
      rule['options'] += [re.findall("\d+", option)]
  
  if rule.get('options') and not rule.get('expr'):
    resolved = "("
    for i, option in enumerate(rule['options']):
      if i != 0:
        resolved += "|"
      for rule_char in option:
        resolved += resolve_rule(rules, rule_char)
    resolved += ")"
    rule['expr'] = resolved
  if rule.get('expr'):
    return rule['expr']


def do(day, part):
  input_name = "example.txt"
  input_name = None
  rules = {}
  messages = None
  for line in common.read_input(day, part, input_name=input_name):
    if messages is None:
      match = re.match("(\d+): (?:\"(\w)\"|((?:\d+[\s\|?]*)+))", line)
      if match:
        label, char, meta = match.groups()
        if char and not meta:
          rules[label] = {"char": char}
        elif meta and not char:
          rules[label] = {"meta": meta}
        else:
          print("Uhoh")
      else:
        messages = [line]
    else:
      messages.append(line)

  labels = list(rules.keys())
  for label in labels:
    rule = rules[label]
    print('resolving', label)
    resolve_rule(rules, label)
  for label, rule in rules.items():
    print(f"{label}: {rule.get('expr')}")

  match_count = 0
  for msg in messages:
    match = re.match("^"+rules["0"]['expr'] + "$", msg)
    if match:
      match_count += 1
      print(f"{msg}: matches - {match}")
    else:
      print(f"{msg}: fails")
  return match_count
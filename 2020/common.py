import os
import requests
import math
from collections.abc import Iterable
AOC_SESSION_TOKEN = os.getenv('AOC_SESSION_TOKEN')
# 10
# 0-1
# -10
# 01

def turn(d, degrees):
  if degrees % 90 != 0:
    raise Exception()
  for x in range(degrees//90):
    d = (d[1], d[0] * -1)
  return d

def face(ch, dirs="NWSE"):
  d = (0,-1)
  return turn(d, dirs.index(ch) * 90)

def move(pos, d, distance=1, dirs="NESW"):
  return (pos[0] + d[0] * distance, pos[1] + d[1] * distance)

def read_input(day, part, input_name=None, group_lines=False):
  if input_name is None:
    response = requests.get(
      f'https://adventofcode.com/2020/day/{day}/input', 
      cookies={"session": AOC_SESSION_TOKEN})
    data = response.text
  else:
    file = open(os.path.abspath(f"./day{day}/{input_name}"))
    data = file.read().replace('\r', '')
  
  group = []
  for line in data.split('\n'):
    if group_lines:
      if len(line) == 0:
        if group:
          yield group
        group = []
      else:
        group.append(line)
    elif line:
      yield(line.replace('\n', '').replace('\r', ''))
  
  if group_lines and group:
    yield group

def iter_dims(dim_slice):
  for i in range(len(dim_slice)):
    if isinstance(dim_slice[i], (list, tuple)):
      for x in iter_dims(dim_slice[i]):
        yield (x[0] + (i,), x[1])
    else:
      yield ((i,), dim_slice[i])

def iter_points(start, size):
  # Iterate points starting from 'start' for 'size' steps
  cur = start
  if len(start) > 1:
    for x in iter_points(start[1:], size[1:]):
      for y in range(start[0], start[0] + size[0]):
        yield (y,) + x
  else:
    for x in range(start[0], start[0] + size[0]):
      yield (x,)

class Space(object):
  def __init__(self, dimensions=None, diminsions=None, n_dims=2, bounded=False):
    if dimensions is None:
      self.dimensions = [0] * n_dims
      self.diminsions = [0] * n_dims
    else:
      self.dimensions = dimensions
      self.diminsions = diminsions
    self.space = {}
    self.bounded = bounded

  def copy(self):
    other = Space(self.dimensions, self.diminsions, bounded=self.bounded)
    other.space = self.space.copy()
    return other

  def append(self, new_slice):
    start = (0,) + tuple(x for x in self.dimensions[1:])
    for point, value in iter_dims(new_slice):
      new_point = tuple()
      for i in range(len(self.dimensions)):
        new_point = new_point + (start[i] + point[i], )
        self.dimensions[i] = max(self.dimensions[i], new_point[i] + 1)
        self.diminsions[i] = min(self.diminsions[i], new_point[i])
      self.space[new_point] = value

  def set(self, value, *args):
    if len(args) == 1 and isinstance(args[0], tuple):
      point = args[0]
    else:
      point = args
    self.space[point] = value
    if not self.bounded:
      for d in range(len(self.dimensions)):
        self.dimensions[d] = max(self.dimensions[d], point[d] + 1)
        self.diminsions[d] = min(self.diminsions[d], point[d])

  def get(self, *args, value=None):
    if len(args) == 1 and isinstance(args[0], tuple):
      point = args[0]
    else:
      point = args
    return self.space.get(point, value)

  def iterate_map(self, pos=None, size=None, extra=None, empty_value=None):
    if pos is None:
      pos = self.diminsions
    elif isinstance(pos, int):
      pos = [x+pos for x in self.diminsions]

    if extra is not None:
      size = [(a-b) + extra for a,b in zip(self.dimensions, pos)]
    elif size is None:
      size = [a-b for a,b in zip(self.dimensions, self.diminsions)]
    elif isinstance(size, int):
      size = [x+size for x in self.dimensions]
    for point in iter_points(pos, size):
      yield point, self.space.get(point, empty_value)    

  def iterate_neighbours(self, pos, size=1, empty_value=None):
    if not isinstance(size, (list, tuple)):
      size = tuple(size for x in range(len(pos)))
    for x in self.iterate_map(
      tuple(a-b for a,b in zip(pos, size)),
      tuple((i*2) + 1 for i in size), empty_value=empty_value):
      if x[0] != pos:
        yield x

  def view_map(self, pos=None, size=None, empty_value=None):
    if pos is None:
      pos = self.diminsions
    if size is None:
      size = [a-b for a,b in zip(self.dimensions, self.diminsions)]

    last_point = None
    line = ""
    space_map = ""
    dim_names = ['x', 'y', 'z', 'a', 'b', 'c']
    for point, value in self.iterate_map(pos, size=size, empty_value=empty_value):
      if last_point is None:
        line += str(value)
      else:
        if last_point is None or last_point[0] < point[0]:
          line += str(value)
        elif last_point[1] < point[1]:
          space_map += line + "\n"
          line = str(value)
        else:
          for i in range(2, len(size)):
            if i == 2:
              space_map += line + "\n"
              line = str(value)
            space_map += (f"{dim_names[i]}= {point[i]}\n")
      last_point = point
    space_map += line + "\n"
    return space_map

class Map(object):
  def __init__(self, width, height):
    self.map = {}
    self.width = width;
    self.height = height;

  def copy(self):
    other = Map(self.width, self.height)
    other.map = self.map.copy()
    return other

  def append(self, line):
    for i in range(len(line)):
      self.map[(i, self.height)] = line[i]
    self.height += 1
    self.width = max(self.width, len(line))

  def set(self, x, y, value):
    self.map[(x,y)] = value

  def get(self, x, y, value=None):
    return self.map.get((x,y), value)

  def get_wrapped(self, x, y, value=None):
    return self.map.get((x % self.width, y % self.height), value)

  def view(self, x, y, width, height, value=None, wrap=True):
    for j in range(y, y + height):
      line = ''.join([
        str(self.get_wrapped(i,j, value)) if wrap else str(self.map.get((i,j), value)) \
        for i in range(x, x + width)
      ])
      print(line)

  def iterate_map(self):
    for y in range(self.height):
      for x in range(self.width):
        yield self.map[(x,y)]

  def iter_view(self, x, y):
    for d in [(0, -1), (1, -1), (1,0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1)]:
      dx, dy = x + d[0], y + d[1]
      while self.get(dx, dy) == '.':
        dx += d[0]
        dy += d[1]
      yield(self.get(dx, dy))


  def iter_surrounds(self, x, y):
    for j in range(max(0, y-1), min(self.height, y + 2)):
      for i in range(max(0, x-1), min(self.width, x + 2)):
        if i != x or j != y:
          yield self.map[(i, j)]

class Game(object):
  def __init__(self):
    self.player_ids = []
    self.players = {}
    self.winner = None

  def add_player(self, player_id, player=None):
    if player is None:
      player = {}
    self.player_ids.append(player_id)
    self.players[player_id] = player

  def round_start(self, game_round):
    pass

  def round_end(self, game_round):
    pass

  def turn_start(self, id, player):
    pass

  def play(self):
    game_round = 0
    while self.winner is None and game_round < 1000:
      game_round += 1
      self.round_start(game_round)
      for player_id in self.player_ids:
        player = self.players[player_id]
        self.turn_start(player_id, player)
      self.round_end(game_round)




import common
import math
tried = [1339]
def do(day, part):
  #sky = common.Map(0,0)
  input_name = "example.txt"
  input_name = None
  pos = (0, 0)
  wp = (10, -1)
  
  for action in common.read_input(day, part, input_name):
    print(action)
    if action[0] in "NWSE":
      md = common.face(action[0])
      wp = common.move(wp, md, distance=int(action[1:]))
    if action[0] == "L":
      wp = common.turn(wp, int(action[1:]))
    if action[0] == "R":
      wp = common.turn(wp, 360 - int(action[1:]))
    if action[0] == "F":
      pos = common.move(pos, wp, distance=int(action[1:]))
    print(pos, wp)

  return abs(pos[0]) + abs(pos[1])

import common
import math
tried = [1339]
def do(day, part):
  #sky = common.Map(0,0)
  input_name = "example.txt"
  input_name = None
  pos = (0, 0)
  d = (1,0)
  
  for action in common.read_input(day, part, input_name):
    print(action)
    if action[0] in "NWSE":
      md = common.face(action[0])
      pos = common.move(pos, md, distance=int(action[1:]))
    if action[0] == "L":
      d = common.turn(d, int(action[1:]))
    if action[0] == "R":
      d = common.turn(d, 360 - int(action[1:]))
    if action[0] == "F":
      pos = common.move(pos, d, distance=int(action[1:]))
    print(pos, d)

  return abs(pos[0]) + abs(pos[1])

import common

def do(day, part):
  groups = []
  for group_lines in common.read_input(day, 1, group_lines = True):
    group = set()
    first = True

    for line in group_lines:
      line_set = set(line)

      if part == 1:
        group.update(line_set)
      else:
        if first:
          first = False
          group.update(line_set)
        else:
          group = group - group.difference(line_set)

    groups.append(group)

  return sum([len(x) for x in groups])

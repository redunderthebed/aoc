import common
import re

class Machine(object):
  def __init__(self, inst_space, data_space):
    self.inst_space = inst_space
    self.data_space = data_space

  def load_insts(self, inst_lines):
    count = 0
    insts = []
    for inst_line in inst_lines:
      match = re.match("(\w+)\s([+-]\d+)", inst_line)
      if match:
        inst = {'code': match.groups()[0], 'params': [int(x) for x in match.groups()[1:]]}
        self.inst_space[count] = inst
        count += 1

  def execute(self, inst_pointer):
    inst = self.inst_space.get(inst_pointer, None)
    if inst.get('code') == 'nop':
      return inst_pointer + 1
    if inst.get('code') == 'acc':
      self.data_space['acc'] += inst.get('params')[0]
      return inst_pointer + 1
    if inst.get('code') == 'jmp':
      return inst_pointer + inst.get('params')[0]

def do(day, part):
  mac = Machine({}, {'acc': 0})
  mac.load_insts(common.read_input(day, part))#, input_name="example.txt"))
  inst_pointer = 0
  visited = set()

  while inst_pointer not in visited:
    visited.add(inst_pointer)
    inst_pointer = mac.execute(inst_pointer)  
  return mac.data_space['acc']


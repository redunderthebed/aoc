import common
import itertools
import math

def do(day, part_number):
  expenses = []
  for x in common.read_input(day, part_number):
    expenses.append(int(x))

  for combination in itertools.combinations(expenses, 3):
    if sum(combination) == 2020:
      return math.prod(combination)
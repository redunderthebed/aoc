import common

def solve_expr(expr):
  expr = expr.split(' ')
  while len(expr) >= 3:
    if expr[1] == "*":
      answer = int(expr[0]) * int(expr[2])
    elif expr[1] == "+":
      answer = int(expr[0]) + int(expr[2])
    expr = [answer] + expr[3:]
  if len(expr) == 1:
    return int(expr[0])
  else:
    raise Exception(f"Still have '{expr}' left")

def solve_expr_with_brackets(expr):
  while True:
    close_b = expr.find(')') + 1
    if close_b == -1:
      break
    open_b = expr[:close_b].rfind('(')
    if open_b == -1:
      break
    bracketed = expr[open_b:close_b][1:-1].split(' ')
    while len(bracketed) >= 3:
      if bracketed[1] == "*":
        answer = int(bracketed[0]) * int(bracketed[2])
      elif bracketed[1] == "+":
        answer = int(bracketed[0]) + int(bracketed[2])
      bracketed = [answer] + bracketed[3:]
    answer = solve_expr(expr[open_b:close_b][1:-1])
    expr = expr[:open_b] + str(answer) + expr[close_b:]
  answer = solve_expr(expr)
  return answer

def do(day, part):
  input_name = None
  total = 0
  for line in common.read_input(day, part, input_name=input_name):
    answer = solve_expr_with_brackets(line)
    print(f"{line} = {answer}")
    total += answer
  return total


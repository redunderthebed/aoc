import common

def do(day, part):
  input_name = "example2.txt"
  input_name = None
  
  numbers = [0] + sorted([int(x) for x in common.read_input(day, part, input_name)])
  max_gap = 3
  numbers.append(numbers[-1] + max_gap)
  
  combinations = 1
  cursor = 0
  trail = []
  while cursor < len(numbers):
    if cursor < len(numbers) - 1:
      diff = numbers[cursor + 1] - numbers[cursor]
    if diff == 1:
      trail.append(diff)
    else:
      delta = 0
      if len(trail) > 1:
        for x in range(1, len(trail)):
          delta += combinations * x
      if delta:
        print('setting', delta)
        combinations += delta
      trail = []
    print(numbers[cursor], trail)
    # print(numbers[cursor:cursor+max_gap+1])
    # for j in range(1, min(max_gap+1, len(numbers)-cursor)):
    #   a = numbers[cursor]
    #   b = numbers[cursor + j]
    #   if b-a <= max_gap:
    #     if j > 1:
    #       added += 1
    #     # if j == 2:
    #     #   add += combinations
    #     # elif j == 3:
    #     #   add += combinations * 2
    #   print(a, b, b-a, "*" if b-a <= max_gap else "")
    # print('added', added)
    # combinations += added * combinations 
    # print(combinations)
    cursor += 1 #max(added, 1)
  return combinations

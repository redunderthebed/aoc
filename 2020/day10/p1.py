import common
from collections import Counter
def do(day, part):
  input_name = "example2.txt"
  input_name = None
  
  numbers = [0] + sorted([int(x) for x in common.read_input(day, part, input_name)])
  numbers.append(numbers[-1] + 3)
  
  gaps = Counter()
  for i in range(1, len(numbers)):
    gaps.update([numbers[i] - numbers[i-1]])
  return gaps[3] * gaps[1]

import collections
import os
import requests
from itertools import tee

AOC_SESSION_TOKEN = os.getenv('AOC_SESSION_TOKEN')


def sliding(iterable, window_size):
    window = tee(iterable, window_size)
    for i in range(1, window_size):
        for j in range(i):
            next(window[i], None)
    return zip(*window)


def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def turn(d, degrees):
    if degrees % 90 != 0:
        raise Exception()
    for x in range(degrees//90):
        d = (d[1], d[0] * -1)
    return d


def face(ch, dirs="NWSE"):
    d = (0, -1)
    return turn(d, dirs.index(ch) * 90)


def move(pos, d, distance=1):
    return pos[0] + d[0] * distance, pos[1] + d[1] * distance


def read_input(day, part, input_name=None, group_lines=False):
    if input_name is None:
        print("hi", AOC_SESSION_TOKEN)
        response = requests.get(
            f'https://adventofcode.com/2016/day/{day}/input',
            cookies={"session": AOC_SESSION_TOKEN})
        data = response.text
        if "Please log in to get your puzzle input." in data:
            raise Exception("Failed to retrieve input data from AOC")
    else:
        file = open(os.path.abspath(f"./day{day}/{input_name}"))
        data = file.read().replace('\r', '')

    group = []
    for line in data.split('\n'):
        if group_lines:
            if len(line) == 0:
                if group:
                    yield group
                group = []
            else:
                group.append(line)
        elif line:
            yield line.replace('\n', '').replace('\r', '')

    if group_lines and group:
        yield group


def iter_dims(dim_slice):
    for i in range(len(dim_slice)):
        if isinstance(dim_slice[i], (list, tuple)):
            for x in iter_dims(dim_slice[i]):
                yield x[0] + (i,), x[1]
        else:
            yield (i,), dim_slice[i]


def iter_points(start, size):
    # Iterate points starting from 'start' for 'size' steps
    if len(start) > 1:
        for x in iter_points(start[1:], size[1:]):
            for y in range(start[0], start[0] + size[0]):
                yield (y,) + x
    else:
        for x in range(start[0], start[0] + size[0]):
            yield tuple([x])


class Space(object):
    def __init__(self, dimensions=None, diminsions=None, n_dims=2, bounded=False):
        if dimensions is None:
            self.dimensions = [0] * n_dims
        else:
            self.dimensions = dimensions
        if diminsions is None:
            self.diminsions = [0] * n_dims
        else:
            self.diminsions = diminsions
        self.space = {}
        self.bounded = bounded

    def copy(self):
        other = Space(self.dimensions, self.diminsions, bounded=self.bounded)
        other.space = self.space.copy()
        return other

    def append(self, new_slice, start=None, direction=None, clean=None):
        pos = start or (0,) + tuple(x for x in self.dimensions[1:])
        direction = direction or (1,) + tuple(0 for x in self.dimensions[1:])
        for value in new_slice:
            self.space[pos] = value if clean is None else clean(value)
            for i in range(len(self.dimensions)):
                self.dimensions[i] = max(self.dimensions[i], pos[i] + 1)
                self.diminsions[i] = min(self.diminsions[i], pos[i])
            pos = self._move(pos, direction, 1)

    def set(self, value, *args):
        if len(args) == 1 and isinstance(args[0], tuple):
            point = args[0]
        else:
            point = args
        self.space[point] = value
        if not self.bounded:
            for d in range(len(self.dimensions)):
                self.dimensions[d] = max(self.dimensions[d], point[d] + 1)
                self.diminsions[d] = min(self.diminsions[d], point[d])

    def get(self, *args, value=None):
        if len(args) == 1 and isinstance(args[0], tuple):
            point = args[0]
        else:
            point = args
        return self.space.get(point, value)

    def iterate_direction(self, pos, direction, reverse_axis=None):
        cur_pos = pos
        while self.is_in_bounds(cur_pos):
            yield cur_pos, self.get(*cur_pos)
            cur_pos = Space.move(cur_pos, direction)
        if reverse_axis is not None:
            reverse = [direction[i] if i != reverse_axis else direction[i] * -1 for i in range(len(direction))]
            cur_pos = Space.move(pos, reverse)
            while self.is_in_bounds(cur_pos):
                yield cur_pos, self.get(*cur_pos)
                cur_pos = Space.move(cur_pos, reverse)

    @staticmethod
    def difference(p1, p2):
        return tuple(p2[i] - p1[i] for i in range(len(p1)))

    @staticmethod
    def move(pos, direction, distance=1):
        return tuple(pos[i] + (direction[i] * distance) for i in range(len(pos)))

    @staticmethod
    def between(min_limit, pos, max_limit):
        return all([min_limit[i] < pos[i] < max_limit[i] for i in range(len(pos))])

    @staticmethod
    def betweeni(min_limit, pos, max_limit):
        return all([min_limit[i] <= pos[i] <= max_limit[i] for i in range(len(pos))])

    def is_in_bounds(self, pos):
        for i in range(len(pos)):
            if pos[i] < self.diminsions[0] or pos[i] > self.dimensions[i]:
                return False
        return True

    def iterate_map(self, pos=None, size=None, extra=None, empty_value=None):
        if pos is None:
            pos = self.diminsions
        elif isinstance(pos, int):
            pos = [x + pos for x in self.diminsions]

        if extra is not None:
            size = [(a - b) + extra for a, b in zip(self.dimensions, pos)]
        elif size is None:
            size = [a - b for a, b in zip(self.dimensions, self.diminsions)]
        elif isinstance(size, int):
            size = [x + size for x in self.dimensions]

        for point in iter_points(pos, size):
            yield point, self.space.get(point, empty_value)

    def iterate_neighbours(self, pos, size=1, empty_value=None, include_self=False):
        if not isinstance(size, (list, tuple)):
            size = tuple(size for _ in range(len(pos)))
        for x in self.iterate_map(
                tuple(a - b for a, b in zip(pos, size)),
                tuple((i * 2) + 1 for i in size), empty_value=empty_value):
            if include_self or x[0] != pos:
                yield x

    def iterate_diamond(self, pos, size=1, empty_value=None):
        if len(self.dimensions) > 2:
            raise Exception(f"Only works for 2 dimensional spaces, found ({len(self.dimensions)})")
        for x in range(-size, size + 1):
            for y in range(-size + abs(x), size - abs(x) + 1):
                new_pos = (pos[0] + x, pos[1] + y)
                value = self.get(*new_pos, value=empty_value)
                yield new_pos, value

    @staticmethod
    def inside(point, minima, maxima):
        for i in range(len(point)):
            if point[i] < minima[i] or point[i] >= maxima[i]:
                return False
        return True

    def get_default_bounds(self, pos, size):
        if pos is None:
            pos = self.diminsions
        if size is None:
            size = [a - b for a, b in zip(self.dimensions, self.diminsions)]
        return pos, size

    def iterate_occupants(self, pos=None, size=None):
        pos, size = self.get_default_bounds(pos, size)
        for item_pos, item_val in self.space.items():
            if Space.inside(item_pos, pos, size):
                yield item_pos, item_val

    def view_map(self, pos=None, size=None, empty_value=None, char_per_pos=1):
        pos, size = self.get_default_bounds(pos, size)

        last_point = None
        line = ""
        space_map = ""
        dim_names = ['x', 'y', 'z', 'a', 'b', 'c']
        for point, value in self.iterate_map(pos, size=size, empty_value=empty_value):
            if last_point is None:
                line += str(value).center(char_per_pos, ' ')
            else:
                if last_point is None or last_point[0] < point[0]:
                    line += str(value).center(char_per_pos, ' ')
                elif last_point[1] < point[1]:
                    space_map += line + "\n"
                    line = str(value).center(char_per_pos, ' ')
                else:
                    for i in range(2, len(size)):
                        if i == 2:
                            space_map += line + "\n"
                            line = str(value)
                        space_map += f"{dim_names[i]}= {point[i]}\n"
            last_point = point
        space_map += line + "\n"
        return space_map


class LookupSpace(Space):
    def __init__(self, dimensions=None, n_dims=2, bounded=False):
        self._lookup = {}
        super(LookupSpace, self).__init__(dimensions=dimensions, n_dims=n_dims, bounded=bounded)

    def set(self, value, *args):
        super(LookupSpace, self).set(value, *args)
        self._lookup[value] = args

    def remove(self, *args):
        value = self.space.pop(args, None)
        self._lookup.pop(value, None)

    def lookup(self, value):
        return self._lookup.get(value, None)


class Map(object):
    def __init__(self, width, height):
        self.map = {}
        self.width = width
        self.height = height

    def copy(self):
        other = Map(self.width, self.height)
        other.map = self.map.copy()
        return other

    def append(self, line):
        for i in range(len(line)):
            self.map[(i, self.height)] = line[i]
        self.height += 1
        self.width = max(self.width, len(line))

    def set(self, x, y, value):
        self.map[(x, y)] = value

    def get(self, x, y, value=None):
        return self.map.get((x, y), value)

    def get_wrapped(self, x, y, value=None):
        return self.map.get((x % self.width, y % self.height), value)

    def view(self, x, y, width, height, value=None, wrap=True):
        for j in range(y, y + height):
            line = ''.join([
                str(self.get_wrapped(i, j, value)) if wrap else str(self.map.get((i, j), value))
                for i in range(x, x + width)
            ])
            print(line)

    def iterate_map(self):
        for y in range(self.height):
            for x in range(self.width):
                yield self.map[(x, y)]

    def iter_view(self, x, y):
        for d in [(0, -1), (1, -1), (1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1)]:
            dx, dy = x + d[0], y + d[1]
            while self.get(dx, dy) == '.':
                dx += d[0]
                dy += d[1]
            yield self.get(dx, dy)

    def iter_surrounds(self, x, y):
        for j in range(max(0, y-1), min(self.height, y + 2)):
            for i in range(max(0, x-1), min(self.width, x + 2)):
                if i != x or j != y:
                    yield self.map[(i, j)]


class Game(object):
    def __init__(self):
        self.player_ids = []
        self.players = {}
        self.winner = None

    def add_player(self, player_id, player=None):
        if player is None:
            player = {}
        self.player_ids.append(player_id)
        self.players[player_id] = player

    def round_start(self, game_round):
        pass

    def round_end(self, game_round):
        pass

    def turn_start(self, player_id, player):
        pass

    def play(self):
        game_round = 0
        while self.winner is None and game_round < 1000:
            game_round += 1
            self.round_start(game_round)
            for player_id in self.player_ids:
                player = self.players[player_id]
                self.turn_start(player_id, player)
            self.round_end(game_round)


class Network(object):
    def __init__(self):
        self.edges = collections.defaultdict(set)
        self.nodes = {}

    def add_connection(self, _from, _to, bidirectional=False):
        self.edges[_from].add(_to)
        self.nodes[_from] = self.nodes.get(_from, None)
        self.nodes[_to] = self.nodes.get(_to, None)

        if bidirectional:
            self.edges[_to].add(_from)

    def get_connections(self, _from):
        return self.edges[_from]

    def has_node(self, node):
        return node in self.nodes

    def set_node_value(self, node, value):
        self.nodes[node] = value

    def get_node_value(self, node):
        return self.nodes[node]


class QuantumVisit(Exception):
    def __init__(self, a, b):
        self.result_a = a
        self.result_b = b
        super(QuantumVisit, self).__init__(f"Quantum Visit ({a}/{b})")


class NetworkQuery(object):
    def __init__(self, visited_filter=None):
        self.visited_filter = visited_filter or (lambda path, node, value, visited: True)

    def _depth_first_nodes(self, network, node, path=None, visited=None):
        if visited is None:
            visited = set()

        path = path or tuple()
        if node not in visited:
            yield node
            if self.visited_filter(path, node, network.get_node_value(node), visited):
                visited = visited.union(set([node]))
            for out in network.get_connections(node):
                for x in self._depth_first_nodes(network, out, path=path + ((node, out),), visited=visited):
                    yield x

    def depth_first_nodes(self, network, node, path=None):
        return list(self._depth_first_nodes(network, node))

    def _iterate_exit_paths(self, network, node, goal, path, visited):
        for out in network.get_connections(node):
            if out not in visited or visited.get(out, 0) > 0:
                if out in visited:
                    visited[out] = max(0, visited.get(out, 0) - 1)
                    if visited[out] == 0:
                        visited[out] = -1
                if out == goal:
                    yield path + ((node, out),)
                else:
                    for x in self._depth_first_paths(network, out, goal, path=path + ((node, out),), visited=visited):
                        yield x

    def _depth_first_paths(self, network, node, goal, path=None, visited=None):
        visited = visited or dict()
        path = path or tuple()

        try:
            result = self.visited_filter(path, node, network.get_node_value(node), visited)
            if result is not None and result is not False:
                visited = visited.copy()
                visited[node] = 0 if result is True else result
        except QuantumVisit as qv:
            a_visited = {node: qv.result_a}
            a_visited.update(visited)
            for x in self._iterate_exit_paths(network, node, goal, path, a_visited):
                yield x
            visited = visited.copy()
            visited[node] = qv.result_b

        for x in self._iterate_exit_paths(network, node, goal, path, visited):
            yield x

    def depth_first_paths(self, network, start, goal):
        return list(self._depth_first_paths(network, start, goal, visited=None, path=None))


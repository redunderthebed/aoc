import argparse
import requests
import os
import pathlib
from bs4 import BeautifulSoup


def build_day(day):
    day_dir = pathlib.Path(f"day{day}")
    if not day_dir.is_dir():
        os.mkdir(day_dir)
        example_file = day_dir / "example"
        with open(example_file, 'w') as file:
            response = requests.request("GET", f"https://adventofcode.com/2016/day/{day}")
            soup = BeautifulSoup(response.text, 'html.parser')
            for p_tag in soup.find_all('p'):
                if "For example" in p_tag.text:
                    example_data = p_tag.find_next_sibling().text
                    print(f"For example:\n{example_data}")
                    file.write(example_data)
        part1 = day_dir / "p1.py"
        if not part1.is_file():
            print("creating p1.py")
            with open(part1, 'w') as file:
                file.write(
                    "import common\n"
                    "\n"
                    "def do(day, part):\n"
                    "    pass\n"
                )


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Build a day directory for AOC")
    parser.add_argument("day", type=int, help="number of the day")
    args = parser.parse_args()
    if args.day == 0:
        for i in range(1, 26):
            build_day(i)
    else:
        build_day(args.day)

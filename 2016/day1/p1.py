import common
import re

from geometry import line_intersect


def do(day, part):
    heading = (0, 1)
    pos = (0, 0)
    lines = set()
    for instr in next(common.read_input(day, part)).split(','):
        match = re.match(f"(\w)(\d+)", instr.strip())
        turn_dir, mag = match.groups()
        heading = common.turn(heading, 90 if turn_dir == 'R' else 270)
        start = common.move(pos, heading, 1)
        pos = common.move(pos, heading, int(mag))
        if part == 2:
            new_line = tuple(sorted((start, pos)))
            for line in lines:
                intersect = line_intersect(*new_line, *line)
                if intersect:
                    return sum([abs(x) for x in intersect])
            lines.add(new_line)
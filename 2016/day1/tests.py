import pytest
from geometry import line_intersect


class TestGeometry(object):
    def test_line_intersect__right_angle(self):
        intersect = line_intersect((1, 0), (1, 2), (0, 1), (2, 1))
        assert intersect == (1, 1)

    def test_line_intersect__right_angle_miss(self):
        intersect = line_intersect((0, 0), (0, 1), (0, 2), (2, 2))
        assert intersect is None

    def test_line_intersect_horizontal(self):
        intersect = line_intersect((0, 0), (5, 0), (2, 0), (5, 0))
        assert intersect == (2, 0)

    def test_line_intersect_horizontal_miss(self):
        intersect = line_intersect((0, 0), (3, 0), (4, 0), (5, 0))
        assert intersect is None

def slope(start, end):
    try:
        return (end[1] - start[1]) / (end[0] - start[0])
    except ZeroDivisionError:
        return None


def segment_intersect(s1, e1, s2, e2):
    if s1 <= s2 <= e1:
        return s2
    elif s2 <= s1 <= e2:
        return s1
    elif s1 <= s2 and e1 >= e2:
        return s2


def line_intersect(l1_start, l1_end, l2_start, l2_end):
    x_int = segment_intersect(l1_start[0], l1_end[0], l2_start[0], l2_end[0])
    y_int = segment_intersect(l1_start[1], l1_end[1], l2_start[1], l2_end[1])
    if x_int is not None and y_int is not None:
        return x_int, y_int